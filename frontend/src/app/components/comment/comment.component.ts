import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { Post } from 'src/app/models/post/post';
import { CommentUpdate } from 'src/app/models/comment/comment-update';
import { CommentService } from 'src/app/services/comment.service';
import { Subject, Observable, empty } from 'rxjs';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';
import { LikeService } from 'src/app/services/like.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public post: Post;
    @Output() deleteCommentRequest = new EventEmitter<Comment>();
    public isUpdating = false;
    public oldBody: string;
    public loading: boolean = false;

    public unsubscribe$ = new Subject<void>();

    public constructor(
        private commentService: CommentService,
        public likeService: LikeService,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deleteComment() {
        this.deleteCommentRequest.emit(this.comment);
    }

    public onEditIconClick() {
        this.isUpdating = true;
        this.oldBody = this.comment.body;
    }

    public saveCommentUpdates() {
        if (this.oldBody == this.comment.body || this.comment.body.trim() == '') {
            this.isUpdating = false;
            return;
        }

        this.loading = true;

        const commentUpdate = {
            id: this.comment.id,
            body: this.comment.body
        } as CommentUpdate;

        this.commentService.updateComment(commentUpdate).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
            this.oldBody = undefined;
            this.isUpdating = false;
            this.loading = false;
        }
        );
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.authDialogService.openAuthDialog(DialogType.SignIn);
                return empty();
            })
        );
    }

    public likeComment(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }
}
