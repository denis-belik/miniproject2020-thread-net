import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from 'src/app/services/post.service';
import { PostShareDto } from 'src/app/models/post/post-share-dto';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
    selector: 'app-share-dialog',
    templateUrl: './share-dialog.component.html',
    styleUrls: ['./share-dialog.component.sass']
})
export class ShareDialogComponent implements OnInit {

    public postId: number;
    public senderName: string;
    public receiverName: string;

    public isEmail = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private postService: PostService,
        private snackBarService: SnackBarService,
        private clipboard: Clipboard) {
        this.postId = data.postId;
        this.senderName = data.senderName;
    }

    ngOnInit(): void {
    }

    public sharePostEmail() {
        const postShareDto: PostShareDto = {
            postId: this.postId,
            senderName: this.senderName,
            receiverName: this.receiverName
        };

        this.postService.sharePostEmail(postShareDto).subscribe(() => {
            this.snackBarService.showUsualMessage("Email has been sent!");
            this.isEmail = false;
        }, (error) => {
            this.snackBarService.showErrorMessage(error);
        });
    }

    public linkToClipboard() {
        this.clipboard.copy(`${window.location.origin}/#${this.postId}`);
        this.snackBarService.showUsualMessage("Copied to clipboard!");
    }
}