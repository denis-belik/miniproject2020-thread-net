import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { UpdatedPost } from '../../models/post/updated-post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { Output, EventEmitter } from '@angular/core';
import { PostService } from '../../services/post.service';
import { ImgurService } from '../../services/imgur.service';
import { ImageService } from '../../services/image.service';
import { MatDialog } from '@angular/material/dialog';
import { ShareDialogComponent } from '../share-dialog/share-dialog.component';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;
    @Output() deletePostRequest = new EventEmitter<Post>();

    public showComments = false;
    public newComment = {} as NewComment;
    public isUpdating = false;
    public oldBody: string;
    public loading = false;
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        public likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private imgurService: ImgurService,
        private imageService: ImageService,
        private dialog: MatDialog
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likePost(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public deletePost() {
        this.deletePostRequest.emit(this.post);
    }

    public onEditIconClick() {
        this.isUpdating = true;
        this.imageService.imageUrl = this.post.previewImage;
        this.oldBody = this.post.body;
    }

    public savePostUpdates() {
        if (this.oldBody == this.post.body && this.imageService.imageUrl == this.post.previewImage) {
            this.isUpdating = false;
            return;
        }

        if (this.post.body.trim() == '' && this.imageService.imageUrl == undefined) {
            this.post.body = this.oldBody;
            this.isUpdating = false;
            return;
        }

        this.loading = true;

        let updatedPost = {} as UpdatedPost;

        let postSubscription;

        if (this.imageService.imageUrl == this.post.previewImage) {
            this.setUpdatedPostModel(updatedPost);
            postSubscription = this.postService.updatePost(updatedPost);
        }
        else if (!this.imageService.imageFile) {
            this.post.previewImage = undefined;
            this.setUpdatedPostModel(updatedPost);
            postSubscription = this.postService.updatePost(updatedPost);
        }
        else {
            postSubscription = this.imgurService.uploadToImgur(this.imageService.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    this.setUpdatedPostModel(updatedPost);
                    return this.postService.updatePost(updatedPost);
                })
            );
        }

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.imageService.removeImage();
                this.loading = false;
                this.isUpdating = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public setUpdatedPostModel(updatedPost: UpdatedPost) {
        updatedPost.id = this.post.id;
        updatedPost.body = this.post.body;
        updatedPost.previewImage = this.post.previewImage;
    }

    public deleteComment(deletedComment: Comment) {
        const deleteSubscription = this.commentService.deleteComment(deletedComment);
        deleteSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                let commentId = this.post.comments.findIndex((comment) => comment.id == deletedComment.id);
                this.post.comments.splice(commentId, 1);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public openShareDialog() {
        let dialogRef = this.dialog.open(ShareDialogComponent, {
            height: '400px',
            width: '300px',
            data: {
                postId: this.post.id,
                senderName: this.currentUser.userName
            }
        });
    }
}
