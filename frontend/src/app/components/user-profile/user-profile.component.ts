import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../models/user';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { UserService } from '../../services/user.service';
import { AuthenticationService } from '../../services/auth.service';
import { ImgurService } from '../../services/imgur.service';
import { switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { UserUpdate } from 'src/app/models/user-update';
import { HubService } from 'src/app/services/hub.service';
import { HubConnectionBuilder } from '@aspnet/signalr';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.sass']
})
export class UserProfileComponent implements OnInit, OnDestroy {
    public user = {} as User;
    public loading = false;
    public imageFile: File;
    public isPwdChanging = false;
    public hidePass = true;
    public password: string;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private userService: UserService,
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private imgurService: ImgurService,
        private hubService: HubService
    ) { }

    public ngOnInit() {
        if (this.hubService.postHub.state != 1) {
            this.hubService.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
            this.hubService.postHub.start()
                .then(() => {
                    this.userService
                        .getUserFromToken()
                        .pipe(takeUntil(this.unsubscribe$))
                        .subscribe((resp) => {
                            this.user = this.userService.copyUser(resp.body);
                            this.hubService.postHub.send("MapClient", resp.body.id);
                        })
                })
                .catch((error) => { console.log(error) });
        } else {
            this.authService
                .getUser()
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) =>
                    (this.user = this.userService.copyUser(user)),
                    (error) => this.snackBarService.showErrorMessage(error));
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public saveNewInfo() {
        const userSubscription = !this.imageFile
            ? this.userService.updateUser(this.mapUser(this.user, this.password))
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.user.avatar = imageData.body.data.link;
                    return this.userService.updateUser(this.mapUser(this.user, this.password));
                })
            );

        this.loading = true;

        userSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.authService.setUser(this.user);
                this.snackBarService.showUsualMessage('Successfully updated');
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public handleFileInput(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            target.value = '';
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.user.avatar = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public mapUser(user: User, password: string): UserUpdate {
        const userUpdate: UserUpdate = {
            id: user.id,
            email: user.email,
            userName: user.userName,
            avatar: user.avatar,
            password: password
        };
        return userUpdate;
    }

    public goBack = () => this.location.back();
}
