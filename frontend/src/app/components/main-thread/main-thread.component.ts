import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { ImageService } from 'src/app/services/image.service';
import { HubService } from 'src/app/services/hub.service';
import { LikeService } from 'src/app/services/like.service';
import { NewReaction } from 'src/app/models/reactions/newReaction';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public skipNumber: number = 0;
    public loadNumber: number = 5;

    public isOnlyMine = false;
    public isFavorite = false;

    public currentUser: User;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;
    public isLoadingAllowed = true; // prevent double loading when two scroll events occur
    public isPostById = false; // true when one post by a link opened

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private imageService: ImageService,
        private hubService: HubService,
        private likeService: LikeService,
        private route: ActivatedRoute
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.hubService.postHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        //this.getPosts();
        this.route.fragment.subscribe((fr) => {
            if (fr) {
                this.isPostById = true;
                this.isLoadingAllowed = false;
                this.getPostById(Number(fr));
            } else {
                this.getSomePosts();
            }
        });

        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = resp.body;
                },
                () => (this.loadingPosts = false)
            );
    }

    public getSomePosts() {
        this.loadingPosts = true;
        this.postService
            .getSomePosts({ skipNumber: this.skipNumber, loadNumber: this.loadNumber })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.isLoadingAllowed = true;
                    this.posts = this.posts.concat(resp.body);
                    this.cachedPosts = this.posts;
                    this.skipNumber = this.posts.length;
                },
                (error) => {
                    this.loadingPosts = false;
                    this.isLoadingAllowed = false;
                    this.snackBarService.showUsualMessage(error);
                }
            );
    }

    public getPostById(postId: number) {
        this.loadingPosts = true;
        this.postService
            .getPostById(postId)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.posts.concat(resp.body);
                    this.cachedPosts = this.posts;
                },
                (error) => {
                    this.loadingPosts = false;
                    this.snackBarService.showErrorMessage(error);
                }
            );
    }

    public sendPost() {
        const postSubscription = !this.imageService.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageService.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.imageService.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public onlyMineSliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.loading = true;

            this.isFavorite = false;
            this.isOnlyMine = true;
            this.postService
                .getPostsByUserId(this.currentUser.id)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((resp) => {
                    this.posts = resp.body;
                    this.loading = false;
                });
        } else {
            this.isOnlyMine = false;
            this.posts = this.cachedPosts;
        }
    }

    public favoriteSliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMine = false;
            this.isFavorite = true;
            this.postService
                .getFavoritePosts(this.currentUser.id)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((resp) => {
                    this.posts = resp.body;
                    this.loading = false;
                });
        } else {
            this.isFavorite = false;
            this.posts = this.cachedPosts;
        }
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.hubService.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });

        this.hubService.postHub.on('DeletePost', (id: number) => {
            this.deletePost(id);
        });

        this.hubService.postHub.on('LikePost', (likedUser: User, reaction: NewReaction) => {
            const post = this.posts.find(post => post.id == reaction.entityId);
            post.reactions = this.likeService.updateLikeIcons(post.reactions, likedUser, reaction.isLike);

            if (reaction.isLike && post.reactions.some(r => r.user == likedUser && r.isLike == reaction.isLike)) {
                let snackBarRef = this.snackBarService.showNotification(`${likedUser.userName} liked your post`);
                snackBarRef.onAction().subscribe(() => {
                    document.getElementById(`${reaction.entityId}`).scrollIntoView();
                });
            }
        })
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }

    public sendDeletedPost(deletedPost: Post) {
        const deleteSubscription = this.postService.deletePost(deletedPost);
        deleteSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.deletePost(deletedPost.id);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public deletePost(id: number) {
        let postId = this.cachedPosts.findIndex((value) => value.id == id);
        this.cachedPosts.splice(postId, 1);
        if (this.isOnlyMine) {
            postId = this.posts.findIndex((value) => value.id == id);
            this.posts.splice(postId, 1);
        }
    }


    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => {
                this.currentUser = user;
            });

    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    @HostListener('window:scroll', ['$event']) onScroll() {
        if (!this.isOnlyMine && !this.isFavorite) {
            const borderDistance = document.body.scrollHeight - (window.scrollY + window.innerHeight);

            if (borderDistance < 100 && this.isLoadingAllowed) {
                this.isLoadingAllowed = false;
                this.getSomePosts();
            }
        }
    }

    public onBackToThread() {
        this.posts.splice(0, this.posts.length);
        this.cachedPosts = this.posts;
        this.isPostById = false;
    }
}
