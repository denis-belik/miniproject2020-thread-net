import { Injectable } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class HubService {

    public postHub: HubConnection;

    constructor(private userService: UserService) {
        this.registerHub();
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start()
            .then(() => {
                this.userService.getUserFromToken().subscribe((resp) => {
                    this.postHub.send("MapClient", resp.body.id);
                })
            })
            .catch((error) => { console.log(error) });
    }
}