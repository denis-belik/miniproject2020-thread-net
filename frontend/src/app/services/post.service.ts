import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { PostShareDto } from '../models/post/post-share-dto';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { UpdatedPost } from '../models/post/updated-post';
import { PostsRangeDto } from '../models/post/posts-range-dto';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public getPostsByUserId(id: number) {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}/user/${id}`);
    }

    public getPostById(id: number) {
        return this.httpService.getFullRequest<Post>(`${this.routePrefix}/${id}`)
    }

    public getFavoritePosts(id: number) {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}/favorite/${id}`);
    }

    public getSomePosts(range: PostsRangeDto) {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}/some`, { skipNumber: range.skipNumber, loadNumber: range.loadNumber })
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public deletePost(post: Post) {
        return this.httpService.deleteRequest<Post>(`${this.routePrefix}/${post.id}`)
    }

    public updatePost(post: UpdatedPost) {
        return this.httpService.putFullRequest(`${this.routePrefix}`, post);
    }

    public sharePostEmail(postShare: PostShareDto) {
        return this.httpService.postFullRequest(`${this.routePrefix}/share/email`, postShare)
    }
}
