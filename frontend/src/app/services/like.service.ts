import { Injectable } from '@angular/core';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Comment } from '../models/comment/comment';
import { CommentService } from './comment.service';
import { Reaction } from '../models/reactions/reaction';
import { MatDialog } from '@angular/material/dialog';
import { LikeDialogComponent } from '../components/like-dialog/like-dialog.component';


@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(
        private postService: PostService,
        private commentService: CommentService,
        private dialog: MatDialog
    ) { }

    public likePost(post: Post, currentUser: User, isLike) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly   
        innerPost.reactions = this.updateLikeIcons(innerPost.reactions, currentUser, isLike);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = this.updateLikeIcons(innerPost.reactions, currentUser, isLike);
                return of(innerPost);
            })
        );
    }

    public likeComment(comment: Comment, currentUser: User, isLike) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly   
        innerComment.reactions = this.updateLikeIcons(innerComment.reactions, currentUser, isLike);

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = this.updateLikeIcons(innerComment.reactions, currentUser, isLike);
                return of(innerComment);
            })
        );
    }

    public updateLikeIcons(reactions: Reaction[], currentUser: User, isLike) {
        let hasSameReaction = reactions.some((x) => x.user.id === currentUser.id && x.isLike === isLike);
        let hasOppositeReaction = reactions.some((x) => x.user.id === currentUser.id && x.isLike !== isLike);

        if (hasSameReaction) {
            reactions = reactions.filter((x) => x.user.id !== currentUser.id);
        }
        else if (hasOppositeReaction) {
            reactions = reactions.filter((x) => x.user.id !== currentUser.id)
            reactions = reactions.concat({ isLike: isLike, user: currentUser });
        }
        else {
            reactions = reactions.concat({ isLike: isLike, user: currentUser });
        }

        return reactions;
    }

    public openLikeDialog(reactions: Reaction[]) {
        const users = this.getLikes(reactions).map(r => r.user);
        this.dialog.open(LikeDialogComponent, {
            data: { users },
            minWidth: 300,
            backdropClass: 'dialog-backdrop',
        });
    }

    public getLikes(reactions: Reaction[]) {
        return reactions.filter(x => x.isLike);
    }

    public getDislikes(reactions: Reaction[]) {
        return reactions.filter(x => !x.isLike);
    }
}
