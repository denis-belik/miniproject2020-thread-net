import { Injectable } from '@angular/core';
import { SnackBarService } from './snack-bar.service';

@Injectable({ providedIn: 'root' })
export class ImageService {
    constructor(
        private snackBarService: SnackBarService
    ) { }

    public imageUrl: string;
    public imageFile: File;

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }
}