import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { CommentUpdate } from '../models/comment/comment-update';
import { NewReaction } from '../models/reactions/newReaction';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public deleteComment(comment: Comment) {
        return this.httpService.deleteFullRequest<Comment>(`${this.routePrefix}/${comment.id}`);
    }

    public updateComment(commentUpdate: CommentUpdate) {
        return this.httpService.putFullRequest<CommentUpdate>(`${this.routePrefix}`, commentUpdate);
    }

    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/like`, reaction);
    }

}
