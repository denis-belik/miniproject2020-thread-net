import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
    selector: '[appUsernameValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: UsernameValidatorDirective, multi: true }]
})
export class UsernameValidatorDirective implements Validator {
    public usernamePattern = '^([0-9A-Za-z]|-|_|\\.)+$';
    public usernameExp = new RegExp(this.usernamePattern);

    validate(control: AbstractControl): { [key: string]: any } | null {
        return ((control: AbstractControl): { [key: string]: any } | null => {
            const isCorrect = this.usernameExp.test(control.value);

            return isCorrect ? null : { 'usernamePattern': { value: control.value } };
        })(control);
    }
}