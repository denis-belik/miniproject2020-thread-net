import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
    selector: '[appEmailValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true }]
})
export class EmailValidatorDirective implements Validator {
    public emailPattern = '^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\.([a-zA-Z]{2,5})$';
    public emailExp = new RegExp(this.emailPattern);

    validate(control: AbstractControl): { [key: string]: any } | null {
        return ((control: AbstractControl): { [key: string]: any } | null => {
            const isCorrect = this.emailExp.test(control.value);
            return isCorrect ? null : { 'emailPattern': { value: control.value } };
        })(control);
    }
}