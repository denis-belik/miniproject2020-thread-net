export interface UserUpdate {
    id: number;
    email: string;
    userName: string;
    avatar: string;
    password: string;
}