export interface CommentUpdate {
    id: number;
    body: string;
}