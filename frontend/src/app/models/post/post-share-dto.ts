export interface PostShareDto {
    postId: number;
    senderName: string;
    receiverName: string;
}
