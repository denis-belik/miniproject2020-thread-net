export interface UpdatedPost {
    id: number;
    previewImage: string;
    body: string;
}
