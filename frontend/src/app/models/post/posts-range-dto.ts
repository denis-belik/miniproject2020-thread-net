export interface PostsRangeDto {
    skipNumber: number;
    loadNumber: number;
}
