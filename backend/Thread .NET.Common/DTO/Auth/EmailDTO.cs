﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.Auth
{
    public class EmailDTO
    {
        public string Email { get; set; }
    }
}
