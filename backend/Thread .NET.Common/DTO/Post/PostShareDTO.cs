﻿namespace Thread_.NET.Common.DTO.Post
{
    public class PostShareDTO
    {
        public int PostId { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
    }
}
