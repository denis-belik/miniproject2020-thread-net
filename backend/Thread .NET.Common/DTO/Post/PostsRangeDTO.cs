﻿namespace Thread_.NET.Common.DTO.Post
{
    public class PostsRangeDTO
    {
        public int SkipNumber { get; set; }
        public int LoadNumber { get; set; }
    }
}
