﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddIsDeletedColumnToCommentEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Comments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 12, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(7059), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(7066), 19 },
                    { 2, 20, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6246), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6262), 19 },
                    { 3, 1, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6336), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6344), 16 },
                    { 4, 12, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6384), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6391), 13 },
                    { 5, 8, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6427), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6434), 5 },
                    { 6, 7, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6470), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6477), 8 },
                    { 7, 18, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6513), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6520), 10 },
                    { 8, 19, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6554), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6561), 4 },
                    { 9, 1, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6607), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6615), 7 },
                    { 10, 8, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6648), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6656), 19 },
                    { 1, 4, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(4602), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(5398), 6 },
                    { 12, 2, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6731), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6738), 4 },
                    { 13, 17, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6773), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6781), 4 },
                    { 14, 3, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6815), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6822), 8 },
                    { 15, 19, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6856), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6863), 1 },
                    { 16, 7, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6896), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6903), 5 },
                    { 17, 10, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6937), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6944), 7 },
                    { 18, 4, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6977), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6985), 7 },
                    { 19, 2, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(7018), false, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(7025), 3 },
                    { 11, 7, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6690), true, new DateTime(2020, 6, 4, 11, 46, 49, 871, DateTimeKind.Local).AddTicks(6697), 19 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Quae libero et non repellat occaecati similique voluptate.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(5709), 3, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(6483) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Ut totam autem recusandae voluptas eos consequuntur qui sint illum.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7443), 17, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7461) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Eum aut repellendus quia iste impedit vel.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7579), 15, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7587) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Cupiditate delectus sed consectetur asperiores cumque cumque.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7669), 10, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7676) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Occaecati explicabo rerum sequi odio.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7744), 9, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7751) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Et exercitationem aut et at explicabo aspernatur.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7825), 15, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7832) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Pariatur et sit veritatis dolorum.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7897), 18, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7905) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Quisquam hic dolores rerum vel voluptatem consequuntur blanditiis aperiam id.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(7998), 13, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8006) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Enim corporis modi sed et et explicabo aut voluptatem.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8086), 2, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8093) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Nisi facilis illo doloremque praesentium voluptatum non ullam totam error.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8175), 10, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8183) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Deleniti natus corporis in veritatis commodi soluta odit.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8257), 12, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8265) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Odit eos consectetur voluptas.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8325), 18, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8333) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Vero aperiam molestiae rem cum architecto nihil in.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8413), 6, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8421) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Ullam impedit nulla assumenda praesentium.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8487), 2, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8495) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Impedit sapiente qui esse at facere et molestiae et.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8572), 6, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8580) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Ut est et animi quam illum aut.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8650), 5, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8657) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Corrupti aut reprehenderit omnis nostrum et veniam laboriosam in labore.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8739), 20, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8747) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 16, "Id magni et quasi.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8805), new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8813) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Quas aut aut est.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8880), 14, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8888) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Et ut rerum earum amet natus id voluptatum fuga.", new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8968), 2, new DateTime(2020, 6, 4, 11, 46, 49, 854, DateTimeKind.Local).AddTicks(8976) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 433, DateTimeKind.Local).AddTicks(5436), "https://s3.amazonaws.com/uifaces/faces/twitter/theonlyzeke/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(6232) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8177), "https://s3.amazonaws.com/uifaces/faces/twitter/duivvv/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8194) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8249), "https://s3.amazonaws.com/uifaces/faces/twitter/ipavelek/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8256) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8286), "https://s3.amazonaws.com/uifaces/faces/twitter/wegotvices/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8293) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8321), "https://s3.amazonaws.com/uifaces/faces/twitter/ripplemdk/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8328) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8355), "https://s3.amazonaws.com/uifaces/faces/twitter/cicerobr/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8362) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8390), "https://s3.amazonaws.com/uifaces/faces/twitter/jydesign/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8396) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8424), "https://s3.amazonaws.com/uifaces/faces/twitter/arashmanteghi/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8431) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8458), "https://s3.amazonaws.com/uifaces/faces/twitter/_yardenoon/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8465) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8491), "https://s3.amazonaws.com/uifaces/faces/twitter/mahmoudmetwally/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8498) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8525), "https://s3.amazonaws.com/uifaces/faces/twitter/vanchesz/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8532) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8559), "https://s3.amazonaws.com/uifaces/faces/twitter/IsaryAmairani/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8566) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8593), "https://s3.amazonaws.com/uifaces/faces/twitter/pixage/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8600) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8627), "https://s3.amazonaws.com/uifaces/faces/twitter/kalmerrautam/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8634) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8661), "https://s3.amazonaws.com/uifaces/faces/twitter/nicolasfolliot/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8668) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8696), "https://s3.amazonaws.com/uifaces/faces/twitter/joannefournier/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8703) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8730), "https://s3.amazonaws.com/uifaces/faces/twitter/aeon56/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8737) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8763), "https://s3.amazonaws.com/uifaces/faces/twitter/alan_zhang_/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8770) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8797), "https://s3.amazonaws.com/uifaces/faces/twitter/christianoliff/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8804) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8832), "https://s3.amazonaws.com/uifaces/faces/twitter/edgarchris99/128.jpg", new DateTime(2020, 6, 4, 11, 46, 49, 434, DateTimeKind.Local).AddTicks(8839) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(3860), "https://picsum.photos/640/480/?image=904", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(4644) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(4978), "https://picsum.photos/640/480/?image=950", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(4995) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5032), "https://picsum.photos/640/480/?image=407", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5040) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5071), "https://picsum.photos/640/480/?image=848", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5078) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5107), "https://picsum.photos/640/480/?image=552", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5114) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5144), "https://picsum.photos/640/480/?image=562", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5151) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5181), "https://picsum.photos/640/480/?image=695", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5188) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5230), "https://picsum.photos/640/480/?image=167", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5237) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5267), "https://picsum.photos/640/480/?image=109", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5274) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5305), "https://picsum.photos/640/480/?image=980", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5312) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5342), "https://picsum.photos/640/480/?image=1056", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5349) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5379), "https://picsum.photos/640/480/?image=1038", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5386) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5415), "https://picsum.photos/640/480/?image=477", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5422) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5452), "https://picsum.photos/640/480/?image=997", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5459) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5489), "https://picsum.photos/640/480/?image=892", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5496) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5526), "https://picsum.photos/640/480/?image=993", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5533) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5562), "https://picsum.photos/640/480/?image=578", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5569) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5598), "https://picsum.photos/640/480/?image=618", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5606) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5635), "https://picsum.photos/640/480/?image=991", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5642) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5672), "https://picsum.photos/640/480/?image=284", new DateTime(2020, 6, 4, 11, 46, 49, 441, DateTimeKind.Local).AddTicks(5678) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1869), true, 9, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1877), 9 },
                    { 11, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1912), true, 12, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1920), 11 },
                    { 12, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1954), false, 9, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1961), 6 },
                    { 13, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1996), false, 16, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2004), 20 },
                    { 15, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2081), true, 16, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2088), 16 },
                    { 18, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2205), false, 7, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2212), 11 },
                    { 16, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2123), false, 14, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2130), 19 },
                    { 17, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2164), false, 6, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2171), 11 },
                    { 9, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1827), false, 6, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1834), 16 },
                    { 20, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2288), false, 9, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2295), 13 },
                    { 19, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2246), true, 4, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2254), 19 },
                    { 14, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2040), false, 5, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(2047), 8 },
                    { 8, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1785), false, 1, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1793), 1 },
                    { 3, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1570), true, 10, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1577), 12 },
                    { 6, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1699), true, 11, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1706), 10 },
                    { 5, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1656), false, 6, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1664), 7 },
                    { 1, new DateTime(2020, 6, 4, 11, 46, 49, 863, DateTimeKind.Local).AddTicks(9912), true, 13, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(625), 5 },
                    { 4, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1614), true, 17, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1621), 10 },
                    { 2, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1497), false, 1, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1515), 5 },
                    { 7, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1742), true, 20, new DateTime(2020, 6, 4, 11, 46, 49, 864, DateTimeKind.Local).AddTicks(1749), 15 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "aperiam", new DateTime(2020, 6, 4, 11, 46, 49, 843, DateTimeKind.Local).AddTicks(4298), 34, new DateTime(2020, 6, 4, 11, 46, 49, 843, DateTimeKind.Local).AddTicks(5057) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Quis voluptatem ullam beatae molestiae laboriosam dolores. Corporis quo ea esse. Omnis ipsa soluta enim amet reprehenderit dignissimos quaerat repellendus alias.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(292), 24, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(323) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Quae et ut quos enim accusamus praesentium. Ducimus ea similique velit non debitis. Blanditiis asperiores porro minima tempore dicta ex autem sapiente. Dolor voluptas qui voluptatem consequuntur qui. Dolores deserunt eos nobis sunt et. Sed corporis distinctio incidunt.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(704), 39, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(712) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Sint perspiciatis dolor omnis soluta dolorem. Eligendi et corporis aut corrupti perspiciatis. Sed voluptatem qui aut repudiandae et id sit rerum consequuntur.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(903), 32, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(911) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Consequuntur totam ut velit unde rerum.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(3383), 34, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(3404) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Voluptas eos ducimus dignissimos ut voluptas expedita in nihil. Voluptates dicta excepturi et et facilis minus. Illo non cupiditate autem doloremque error est a. Earum animi non in quis sit voluptatem officia. Nostrum quod et commodi voluptates qui. Rerum numquam vel fugiat ut recusandae ullam recusandae veritatis saepe.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(3744), 32, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(3752) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, @"Libero enim beatae porro aut numquam eos sed quasi adipisci.
Dolores aliquid asperiores eaque est ullam.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5398), 39, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5418) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "aliquid", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5512), 28, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5521) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Ullam minima laboriosam.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5595), 30, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5602) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "illo", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5646), 28, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5654) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "nemo", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5698), 26, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5705) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, @"Cum enim quidem accusamus quibusdam rerum itaque voluptatem.
Exercitationem quia nobis delectus.
Minima consectetur id impedit tempore itaque et.
Est qui nihil.
Rerum et minus ea odit voluptas fugit qui tempore.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5929), 22, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(5936) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Ex laboriosam ipsam ad.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(6003), 23, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(6011) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Debitis autem sed omnis atque itaque.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(6540), 36, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(6560) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Ducimus aperiam alias perferendis quis et.
Repellat sunt sunt vero.
Nihil voluptatem dolores voluptatem.
Corrupti repellendus occaecati velit illo ea et.
Et at dolorum iure.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7197), 27, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7222) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "esse", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7364), 40, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7373) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "aliquid", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7425), 27, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7432) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Minus quibusdam fugiat sit deserunt velit quis doloribus dolorum velit.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7527), 30, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7535) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "dicta", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7593), 28, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7600) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Rerum similique est aliquam sed aut repellendus porro. Voluptatem ut enim praesentium. Culpa soluta aliquam non quae. Sed similique harum assumenda sed asperiores eius. Ut et quas velit odio. Non quis quia perspiciatis quo.", new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7832), 25, new DateTime(2020, 6, 4, 11, 46, 49, 846, DateTimeKind.Local).AddTicks(7840) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 4, 11, 46, 49, 571, DateTimeKind.Local).AddTicks(7456), "Dandre.Schaefer79@yahoo.com", "ielVQBshsCT0dwMt4uDINhQ2xLQS0823NwFsD8NUAjY=", "iJC0HKgMRaHuEzlHXA7l2VAOTNb6JWPAzPhLrIexv6o=", new DateTime(2020, 6, 4, 11, 46, 49, 571, DateTimeKind.Local).AddTicks(8278), "Clovis.Zboncak" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 4, 11, 46, 49, 584, DateTimeKind.Local).AddTicks(4045), "Ansel_Kessler@yahoo.com", "mgcL3+XA/W8hiVffdfsFnnzMZAHh+4GwyAmLqR7OnN4=", "qkBUa15534ssgyZjB3FzQMQ53WksX3Sbf9kKTjnW6bA=", new DateTime(2020, 6, 4, 11, 46, 49, 584, DateTimeKind.Local).AddTicks(4087), "Javonte6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 4, 11, 46, 49, 599, DateTimeKind.Local).AddTicks(9400), "Ofelia_OConnell19@hotmail.com", "/8QpBYZV/TUgCnULywnKcYOabFyiznWfcLeYEPUTYFI=", "BFFGXIFy5/MzrGX7j+F14k9hOk/S7DZ4yLs1x9GUR3g=", new DateTime(2020, 6, 4, 11, 46, 49, 599, DateTimeKind.Local).AddTicks(9425), "Lewis_Kuphal71" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 4, 11, 46, 49, 612, DateTimeKind.Local).AddTicks(3011), "Josue.Leuschke7@hotmail.com", "cUPHNo6t87UvVVh64u4Lmd4L8+HUQK6KBm7Rmu10M74=", "eyPt767SOqg3SwtQW8tj0QCqhluvsX8pJaZhdj1caV4=", new DateTime(2020, 6, 4, 11, 46, 49, 612, DateTimeKind.Local).AddTicks(3032), "Saige.Bradtke" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 4, 11, 46, 49, 624, DateTimeKind.Local).AddTicks(7071), "Hope34@hotmail.com", "fY5Ue2lOcEEFGJFWfeXVpSdtHAOhBXxy1YAt8oQkIGM=", "T3sZ4112BdbUZpGM1BCNL2IM7PJyzm5CSux7pTRzFQw=", new DateTime(2020, 6, 4, 11, 46, 49, 624, DateTimeKind.Local).AddTicks(7095), "Kattie66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 4, 11, 46, 49, 637, DateTimeKind.Local).AddTicks(379), "Chance14@gmail.com", "m6koWXCyVvP+lI08ubSBuPvaYsK+4avqv7AEfBkUXZY=", "ZpW03pVmPs+bCtgHR7Tj+7zn8K50TfCZxuRO5HVzI5k=", new DateTime(2020, 6, 4, 11, 46, 49, 637, DateTimeKind.Local).AddTicks(404), "Eloy.Windler24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 4, 11, 46, 49, 649, DateTimeKind.Local).AddTicks(4712), "Hattie_Lesch98@gmail.com", "cWxgo86JHxUTVR7jSvuebact8mUpEaCeaOrIAwsB5ow=", "T4tNObYYpPo3t/Uq2tPAhGIGAk54cpwk209UwT60fgE=", new DateTime(2020, 6, 4, 11, 46, 49, 649, DateTimeKind.Local).AddTicks(4733), "Anabel_Gorczany" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 4, 11, 46, 49, 661, DateTimeKind.Local).AddTicks(7726), "Nikolas.Ryan3@gmail.com", "1N6/X5HY2G+fi7iXhoUTAfHL75VsRqm8fJ6z1BrZ9Uw=", "IFxCQHyHB87wYXWcGfMVXUEHn6VVIKBmIycrTsyI/e4=", new DateTime(2020, 6, 4, 11, 46, 49, 661, DateTimeKind.Local).AddTicks(7744), "Tyreek14" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 4, 11, 46, 49, 674, DateTimeKind.Local).AddTicks(587), "Johnny28@gmail.com", "yBonS2OK+HWCOBJoavAPEADh0uUgQuvE+JtZvhBXHKo=", "KTbi+rcYtuK3VdxDhOj51CzsCDyQY8a684J/xrjytBE=", new DateTime(2020, 6, 4, 11, 46, 49, 674, DateTimeKind.Local).AddTicks(610), "Jaclyn_Macejkovic" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 4, 11, 46, 49, 686, DateTimeKind.Local).AddTicks(3932), "Antonio_Beahan43@gmail.com", "LOQj9lVf9NExndxLDPZ2kiMO0IYQfyOT+DeA/JEyKEg=", "pBfnkrazazp96T0zYcmMubD9h3XfMh7iiLyt4nzmibk=", new DateTime(2020, 6, 4, 11, 46, 49, 686, DateTimeKind.Local).AddTicks(3951), "Keon_Gutmann31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 4, 11, 46, 49, 698, DateTimeKind.Local).AddTicks(7151), "Baron_Farrell@gmail.com", "3B5K6WRFa37aTbncIROX34UVKHan70iCRGNqA5DgiQI=", "7vVsuuoaVWARnp90wRYYL1Tfm9J0idGxZGxPUU1Zp90=", new DateTime(2020, 6, 4, 11, 46, 49, 698, DateTimeKind.Local).AddTicks(7169), "Emmitt_DuBuque87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 4, 11, 46, 49, 710, DateTimeKind.Local).AddTicks(9773), "Kailyn8@hotmail.com", "4mrDuRR42vUPjDf+l8H0oY+Oqs+IreUHO20VE2Oa+ZQ=", "mJ2Ryed9GCvho0IeIHmzBmXnwtZ9dzFpiwspzzOzYns=", new DateTime(2020, 6, 4, 11, 46, 49, 710, DateTimeKind.Local).AddTicks(9797), "Lurline_Heller" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 4, 11, 46, 49, 723, DateTimeKind.Local).AddTicks(2588), "Anita.Dare73@gmail.com", "IINFF+IFW4l8Vg/6CTkF3aWqVqpAeVFEnmZySLIa/EU=", "aHdGaFTz97PXewUa5txOgHHAFTCZ3AYuJJb8rvIWJZM=", new DateTime(2020, 6, 4, 11, 46, 49, 723, DateTimeKind.Local).AddTicks(2612), "Favian87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 4, 11, 46, 49, 735, DateTimeKind.Local).AddTicks(6012), "Nia_Ritchie48@gmail.com", "H5YwpWaYojWNPxL0eI/Dh0LwwbgRJGyNvzDfEpDIVac=", "VV9TDnE7jZaG4bdbx7tuSFDSjkN6TihvYVrc5Iu4EQo=", new DateTime(2020, 6, 4, 11, 46, 49, 735, DateTimeKind.Local).AddTicks(6039), "Jalen.Mosciski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 4, 11, 46, 49, 747, DateTimeKind.Local).AddTicks(9767), "Elizabeth75@yahoo.com", "pqjE7GvNCQoj9TJNx1kkjhV9HDivA7jRDwf2WszRoxA=", "X/6Jq/L5cfSaVDnRQFAMftj8LBA+bbzmDllxSHAE+YE=", new DateTime(2020, 6, 4, 11, 46, 49, 747, DateTimeKind.Local).AddTicks(9786), "Emile_Hagenes82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 4, 11, 46, 49, 760, DateTimeKind.Local).AddTicks(2256), "Francesca.Schimmel33@hotmail.com", "BGpEsyDxOOUuQ78HiSkwrCwSim/5PEbPbhRLCjM8M1c=", "50YEWAelfckcifJrN9PNfrfGsz72gfUlxm3frqQCg64=", new DateTime(2020, 6, 4, 11, 46, 49, 760, DateTimeKind.Local).AddTicks(2281), "Wellington_Kutch9" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 4, 11, 46, 49, 772, DateTimeKind.Local).AddTicks(4788), "Maye.Klocko@hotmail.com", "kSFEzCrkIc+MNfm8TYSkCNbbMYVf2kMyKU7dUQcDtNM=", "2ttV7HsD4ht39IWaK4ROifHhVOIba7GFAL3hioxL+Cs=", new DateTime(2020, 6, 4, 11, 46, 49, 772, DateTimeKind.Local).AddTicks(4814), "Charity.Luettgen51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 4, 11, 46, 49, 784, DateTimeKind.Local).AddTicks(7373), "Lynn_Rice67@hotmail.com", "FMnn7M3KtbUJSaoOg3+xTi20az9p5Dktk6cGDDq30Eo=", "prAXwAzEC3XT2rYUsrVtSrXOIiW5TNwKe0BQg44DRrM=", new DateTime(2020, 6, 4, 11, 46, 49, 784, DateTimeKind.Local).AddTicks(7396), "Sidney_Ziemann50" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 4, 11, 46, 49, 797, DateTimeKind.Local).AddTicks(830), "Micah.Bartell48@gmail.com", "3vSxUYYn3UgmyY+d2HeIP3zftY2kxbR3OxKfTKcWjTA=", "EJSvgG2sTAF8DlXrpgEOWtW7WMOQvFpgzzPeN/f4Ikg=", new DateTime(2020, 6, 4, 11, 46, 49, 797, DateTimeKind.Local).AddTicks(851), "Ernie_Satterfield" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 4, 11, 46, 49, 809, DateTimeKind.Local).AddTicks(3249), "Emma52@hotmail.com", "MeyXmKbM0DIoKdunP1H5q4Hzzh2ZjdO6V0e2r/dofRk=", "uIibTIbXap8cMOiyCFs/VY0ErAb6wXW6xJmRtQkrmxA=", new DateTime(2020, 6, 4, 11, 46, 49, 809, DateTimeKind.Local).AddTicks(3273), "Marlin.Kautzer14" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 4, 11, 46, 49, 821, DateTimeKind.Local).AddTicks(6497), "tyscULYcJxw2vG/FsY+dAQtJ2zLxq8nglUedG+uatFQ=", "dgSyTuhypcgxnJkQG7B9gTmgyMirLEhhb+Xojelc7FA=", new DateTime(2020, 6, 4, 11, 46, 49, 821, DateTimeKind.Local).AddTicks(6497) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Comments");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 17, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2774), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2781), 2 },
                    { 2, 10, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2019), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2037), 12 },
                    { 3, 7, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2088), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2096), 19 },
                    { 4, 20, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2131), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2139), 18 },
                    { 5, 15, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2173), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2180), 13 },
                    { 6, 6, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2214), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2221), 4 },
                    { 7, 14, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2255), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2262), 15 },
                    { 8, 2, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2296), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2303), 6 },
                    { 9, 14, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2336), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2343), 16 },
                    { 10, 10, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2376), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2383), 2 },
                    { 1, 13, new DateTime(2020, 5, 28, 17, 35, 32, 354, DateTimeKind.Local).AddTicks(9976), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(1186), 12 },
                    { 12, 2, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2455), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2462), 9 },
                    { 13, 13, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2494), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2501), 2 },
                    { 14, 16, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2535), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2542), 7 },
                    { 15, 6, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2574), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2582), 21 },
                    { 16, 9, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2614), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2621), 6 },
                    { 17, 5, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2654), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2661), 12 },
                    { 18, 13, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2694), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2701), 3 },
                    { 19, 6, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2734), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2741), 9 },
                    { 11, 2, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2416), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2422), 15 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Nostrum consectetur animi dicta nesciunt veritatis quos similique et beatae.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(2964), 10, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(4137) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Eaque sunt eligendi.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5062), 7, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5080) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Eos occaecati eligendi.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5168), 8, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5176) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Enim incidunt sed aut.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5278), 1, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5286) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Maxime dolorem laboriosam quaerat distinctio rerum iure nobis aut quisquam.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5385), 11, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5393) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Aperiam nostrum id repudiandae consequuntur exercitationem.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5466), 19, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5473) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Rem animi esse quia et nulla.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5544), 8, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5551) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Nulla eaque autem qui vero optio.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5621), 1, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5628) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Rerum qui magni et animi ut a voluptatem voluptatem et.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5726), 20, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5734) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Quas quasi minima optio quod voluptas blanditiis voluptatibus.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5856), 18, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5864) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Magnam magnam quas veritatis nemo iusto et animi.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5940), 7, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5947) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Sint sit libero alias dolor dolore facere.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6019), 5, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6026) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Quos sit sed corporis.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6086), 4, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6093) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Voluptatum sunt voluptatibus voluptate ut dolores omnis ut minus saepe.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6184), 12, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6191) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Id debitis sit sint omnis hic accusantium quod.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6269), 5, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6276) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Porro est totam possimus.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6337), 20, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6344) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Vero ad hic consequuntur non aliquid sit reiciendis fugiat.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6424), 14, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6431) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, "Ex voluptates ut deleniti natus et.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6498), new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6506) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Dolores dolores numquam vel commodi possimus est saepe.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6581), 16, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6588) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Voluptas dolores maiores ipsam est.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6661), 7, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6668) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 910, DateTimeKind.Local).AddTicks(335), "https://s3.amazonaws.com/uifaces/faces/twitter/kevinjohndayy/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(1880) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3560), "https://s3.amazonaws.com/uifaces/faces/twitter/stefvdham/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3578) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3631), "https://s3.amazonaws.com/uifaces/faces/twitter/0therplanet/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3670), "https://s3.amazonaws.com/uifaces/faces/twitter/begreative/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3677) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3705), "https://s3.amazonaws.com/uifaces/faces/twitter/okandungel/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3712) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3741), "https://s3.amazonaws.com/uifaces/faces/twitter/serefka/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3748) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3775), "https://s3.amazonaws.com/uifaces/faces/twitter/quailandquasar/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3782) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3809), "https://s3.amazonaws.com/uifaces/faces/twitter/depaulawagner/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3817) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3845), "https://s3.amazonaws.com/uifaces/faces/twitter/boxmodel/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3852) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3880), "https://s3.amazonaws.com/uifaces/faces/twitter/plasticine/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3887) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3918), "https://s3.amazonaws.com/uifaces/faces/twitter/cynthiasavard/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3925) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3953), "https://s3.amazonaws.com/uifaces/faces/twitter/davidsasda/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3960) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3989), "https://s3.amazonaws.com/uifaces/faces/twitter/lvovenok/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4023), "https://s3.amazonaws.com/uifaces/faces/twitter/nellleo/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4030) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4058), "https://s3.amazonaws.com/uifaces/faces/twitter/lososina/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4065) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4092), "https://s3.amazonaws.com/uifaces/faces/twitter/thierrykoblentz/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4099) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4127), "https://s3.amazonaws.com/uifaces/faces/twitter/hgharrygo/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4134) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4163), "https://s3.amazonaws.com/uifaces/faces/twitter/mvdheuvel/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4170) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4197), "https://s3.amazonaws.com/uifaces/faces/twitter/carlosjgsousa/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4204) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4233), "https://s3.amazonaws.com/uifaces/faces/twitter/nyancecom/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4240) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(5739), "https://picsum.photos/640/480/?image=523", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(6995) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7317), "https://picsum.photos/640/480/?image=988", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7333) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7374), "https://picsum.photos/640/480/?image=1050", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7382) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7413), "https://picsum.photos/640/480/?image=761", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7450), "https://picsum.photos/640/480/?image=880", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7457) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7487), "https://picsum.photos/640/480/?image=883", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7494) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7524), "https://picsum.photos/640/480/?image=325", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7531) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7561), "https://picsum.photos/640/480/?image=506", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7568) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7737), "https://picsum.photos/640/480/?image=370", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7744) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7777), "https://picsum.photos/640/480/?image=233", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7784) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7816), "https://picsum.photos/640/480/?image=50", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7823) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7853), "https://picsum.photos/640/480/?image=504", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7860) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7890), "https://picsum.photos/640/480/?image=165", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7897) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7927), "https://picsum.photos/640/480/?image=668", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7934) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7965), "https://picsum.photos/640/480/?image=1055", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7972) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8001), "https://picsum.photos/640/480/?image=424", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8009) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8038), "https://picsum.photos/640/480/?image=507", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8045) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8074), "https://picsum.photos/640/480/?image=911", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8081) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8111), "https://picsum.photos/640/480/?image=290", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8118) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8148), "https://picsum.photos/640/480/?image=973", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8155) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(247), false, 19, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(254), 5 },
                    { 11, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(287), false, 2, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(294), 15 },
                    { 12, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(328), true, 18, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(335), 19 },
                    { 13, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(370), true, 5, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(377), 8 },
                    { 15, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(451), true, 15, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(458), 5 },
                    { 18, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(577), true, 4, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(583), 1 },
                    { 16, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(491), true, 2, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(498), 14 },
                    { 17, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(534), true, 13, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(541), 3 },
                    { 9, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(207), false, 8, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(214), 7 },
                    { 20, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(656), true, 12, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(663), 7 },
                    { 19, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(616), true, 5, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(623), 9 },
                    { 14, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(412), true, 11, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(419), 13 },
                    { 8, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(167), false, 2, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(175), 11 },
                    { 3, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9951), true, 7, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9960), 18 },
                    { 6, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(84), true, 3, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(92), 8 },
                    { 5, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(42), true, 7, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(49), 5 },
                    { 1, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(7743), false, 12, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(8937), 16 },
                    { 4, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(1), true, 19, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(8), 4 },
                    { 2, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9841), true, 2, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9858), 20 },
                    { 7, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(126), false, 4, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(133), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "a", new DateTime(2020, 5, 28, 17, 35, 32, 322, DateTimeKind.Local).AddTicks(1273), 22, new DateTime(2020, 5, 28, 17, 35, 32, 322, DateTimeKind.Local).AddTicks(2512) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Et iste quia molestiae aut facilis. Autem consequatur voluptatibus facilis eum deserunt nulla suscipit et. Perspiciatis delectus et quidem. Consequatur aut delectus eos nihil enim molestiae.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(444), 38, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(477) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Ipsam esse quae excepturi aperiam excepturi inventore.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3502), 29, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3523) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Eaque reprehenderit eum odit quisquam quo repudiandae voluptatem pariatur.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3669), 37, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3677) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Aspernatur praesentium ratione earum quas hic molestiae est.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3769), 27, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3777) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Quis qui quia aperiam et dolore.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3864), 22, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3872) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "quasi", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3972), 32, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3980) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Ut odit laudantium placeat error architecto ullam esse.
Commodi sed ut dolorum blanditiis accusantium nihil.
Unde tempore et maiores autem fugit earum.
Soluta nesciunt nobis accusantium necessitatibus aut laboriosam.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6159), 40, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6180) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Architecto quo ex doloribus ipsam omnis libero temporibus ut. Est aperiam consequatur consequatur corrupti molestiae. Est dolor doloribus. Sit rerum repudiandae laborum iure soluta aliquam. Earum vel et voluptate quo quos quo ipsam odio.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6609), 34, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6618) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Numquam ut iusto est.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6692), 34, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6700) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Excepturi dolor et nisi saepe eum dignissimos autem molestiae.
Esse aut ea et tempora et provident excepturi et.
Cum illo itaque ut quis delectus est.
Sapiente minus quibusdam harum commodi aut.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6920), 38, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6928) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Consequatur officiis et maxime quis aperiam qui. Ipsum quia sit quia quis aut aut neque blanditiis. Dicta ut molestiae dolor sapiente velit adipisci quasi dolor. Corporis adipisci molestias at ipsa repudiandae facilis id.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7138), 33, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7146) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, @"Sequi deleniti enim earum.
Molestiae distinctio natus earum velit officiis est.
Cum eum voluptates et omnis ullam magnam.
Molestias aut optio dolorem vitae blanditiis.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7323), 27, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Culpa quod assumenda optio eligendi.
Officia rerum quia dicta ea repellendus.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7435), 33, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7442) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Dolorem sapiente omnis ut quaerat minus est molestias sunt.
Cupiditate aut sit quos reprehenderit ut mollitia aut omnis.
Magni eius sit sapiente expedita consequuntur velit voluptatem consectetur veritatis.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7624), 21, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7632) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "excepturi", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7678), 25, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7685) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Voluptatem consequatur quia.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7746), 24, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7754) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Autem deserunt delectus sint eaque ab quod sit.
Id omnis quibusdam est magni quia dolores ipsam.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7874), 31, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7881) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Dolore aut aut officia temporibus est repellendus itaque aut. Ut doloribus suscipit illum sit tempore. Tenetur qui rerum. Rerum consequatur officia qui consectetur nobis fuga officiis est. Aspernatur voluptates aut ut neque harum ducimus voluptatem ut unde. Et quis saepe dolorum quibusdam quas enim facilis.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8157), 30, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8164) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "officiis", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8209), 40, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8216) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 5, 28, 17, 35, 32, 55, DateTimeKind.Local).AddTicks(781), "Rebeca30@gmail.com", "RyvonGHE/Pjdxfgl5qiJrjbAEjnwvMkoAC2x877hpFA=", "Tu8+jVko0KAd/Q8nJk8uLZub2w+kn9PEVlrdIFhBHIc=", new DateTime(2020, 5, 28, 17, 35, 32, 55, DateTimeKind.Local).AddTicks(2063), "Clara92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 5, 28, 17, 35, 32, 68, DateTimeKind.Local).AddTicks(727), "Roderick_Roob4@gmail.com", "8+XT0XcrvXcdZlMAyVFYfV5f/BDKqlSnFppPyVD+cLk=", "Wk6u7URMjtOxz3jx59rPZFJr8zc2H/Dez1hFh4gTSbM=", new DateTime(2020, 5, 28, 17, 35, 32, 68, DateTimeKind.Local).AddTicks(773), "Davin_Kilback29" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 5, 28, 17, 35, 32, 80, DateTimeKind.Local).AddTicks(5989), "Cody29@yahoo.com", "1cC8bbkdUk2w5HamP/UrNThOPZvuocUqgA0OIUCOh2Q=", "Ilcz1KWXXvgOy9NVPuv7wyW5VYhvigk5eKp/AP36X6A=", new DateTime(2020, 5, 28, 17, 35, 32, 80, DateTimeKind.Local).AddTicks(6027), "Jeanie.Christiansen" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 5, 28, 17, 35, 32, 93, DateTimeKind.Local).AddTicks(1344), "Ora.Nienow@gmail.com", "Ul4GWuqjSLTX653Te31M11UGehESwC4TfpXi92PHAl8=", "GBt9i72yHVEonOQ7yODXXTgg7CqkLeLp2YhnQJySavY=", new DateTime(2020, 5, 28, 17, 35, 32, 93, DateTimeKind.Local).AddTicks(1380), "Ansley_Kuphal" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 5, 28, 17, 35, 32, 105, DateTimeKind.Local).AddTicks(4530), "Florida_Sanford@gmail.com", "kYzPa+P9ckxCQTdf3kDShgD0pznl5Jz9EvG4cJotS84=", "TqyZL1ga8x1BpqweXtrGrKdF2M5DGRuHm2oYNGFzyVE=", new DateTime(2020, 5, 28, 17, 35, 32, 105, DateTimeKind.Local).AddTicks(4546), "Sandra_Brekke95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 5, 28, 17, 35, 32, 117, DateTimeKind.Local).AddTicks(7633), "Mark53@hotmail.com", "/wO6LKs7GUwFhTUH5gTeQ85ATAzQhOXTiVbgIHnstWA=", "tecffbB6g+fYb4fkFx8DmpARf23MMfYcmQF3jtg4pwQ=", new DateTime(2020, 5, 28, 17, 35, 32, 117, DateTimeKind.Local).AddTicks(7662), "Jalon23" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 130, DateTimeKind.Local).AddTicks(848), "Lamar97@hotmail.com", "ziLV4QKhaDo4GfMynoq8H/3a8vUUOJHY5yrNowam810=", "08bzRCCF6gUxios0aKhvtxy4YL+YaI5NUClaPNAOqmc=", new DateTime(2020, 5, 28, 17, 35, 32, 130, DateTimeKind.Local).AddTicks(867), "Arely49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 5, 28, 17, 35, 32, 142, DateTimeKind.Local).AddTicks(3795), "Jeramie2@gmail.com", "vz2CtwJ1xi3pKCOP8iAODTM5ktjpvmoZ/MhbI592UoA=", "9oYHIm6n4lvKH46uS+ttOLz+4L913FzNqw0e6RtSbQU=", new DateTime(2020, 5, 28, 17, 35, 32, 142, DateTimeKind.Local).AddTicks(3827), "Shany.Padberg63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 5, 28, 17, 35, 32, 154, DateTimeKind.Local).AddTicks(6267), "Rachel6@gmail.com", "eF/Kzf+TkcRB3T5yT3nsQa8H3mTIpghzFpWyuguMZR4=", "VN8PA7BYitUum6BaosNSA0D26xVqK8riDWSV+tic7UM=", new DateTime(2020, 5, 28, 17, 35, 32, 154, DateTimeKind.Local).AddTicks(6280), "Reese64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 5, 28, 17, 35, 32, 166, DateTimeKind.Local).AddTicks(8669), "Dustin5@yahoo.com", "dDKUfhyoVIL0xwL2xtRAuh/osD4oV6OCid0vx+NCXdA=", "r3K4RpOQHZHoBZ9sXE6G8GX1skhAxhHw9++Hz8/1/m4=", new DateTime(2020, 5, 28, 17, 35, 32, 166, DateTimeKind.Local).AddTicks(8681), "Jonathan_Durgan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 5, 28, 17, 35, 32, 179, DateTimeKind.Local).AddTicks(1184), "Madisyn50@yahoo.com", "zh7huxkv6ZxnlG/YKiUPZF5AfP65Xg4Pft4ZzUVYFUE=", "afp22J/V8RrSXy8QtXTCGa3ho7kLdTDx0/ES92n+WPs=", new DateTime(2020, 5, 28, 17, 35, 32, 179, DateTimeKind.Local).AddTicks(1203), "Hoyt_Pfeffer98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 5, 28, 17, 35, 32, 191, DateTimeKind.Local).AddTicks(3625), "Imani58@gmail.com", "+mxgITa4n1fqMbJVs5B2T/elwQFur3RwPXTjtnQPzlE=", "hLlS9pDeqDEn/O4iKfJclsUyPY+airs+Qg0hkpqskt4=", new DateTime(2020, 5, 28, 17, 35, 32, 191, DateTimeKind.Local).AddTicks(3639), "Tristin.Mitchell59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 5, 28, 17, 35, 32, 203, DateTimeKind.Local).AddTicks(6152), "Hilton.Quitzon16@hotmail.com", "yw3eQWq5jzWQfs4m0+sw93AiPDqxVrfLkrLgnkVp5R8=", "MXnjhnaI5lr7s82pHrDfehmc/pb9ewJ/yBWLZRUtBvE=", new DateTime(2020, 5, 28, 17, 35, 32, 203, DateTimeKind.Local).AddTicks(6164), "Kristy32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 5, 28, 17, 35, 32, 215, DateTimeKind.Local).AddTicks(8462), "Brannon_Kertzmann45@yahoo.com", "cIZWwZ232uLJuXgdOX6iIab8XNPtIjVSxNPKiAHwKmI=", "6yilvpIBWha2+qbLQYKTwcMI55fnW5QP6aiXPuWSX7Y=", new DateTime(2020, 5, 28, 17, 35, 32, 215, DateTimeKind.Local).AddTicks(8472), "Graham96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 228, DateTimeKind.Local).AddTicks(2724), "Elvie19@hotmail.com", "gvYMb2UlS6JzvPNJX0iyZBA4sflMuXnE+IArIgIZato=", "b0TDNpCRUE4akEMC4yCE5+ZvL233A47VePLY/oAfDgE=", new DateTime(2020, 5, 28, 17, 35, 32, 228, DateTimeKind.Local).AddTicks(2761), "Mae85" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 5, 28, 17, 35, 32, 243, DateTimeKind.Local).AddTicks(9844), "Justyn_OConner@hotmail.com", "PQ9RCb1BrSuYyC6HUCMlVKhV9SkkTpO9hikhF9NVwwE=", "bz/DJRSxJDFrr2i8+8wuHniZ+LKjVNDPvvAj7aKY++E=", new DateTime(2020, 5, 28, 17, 35, 32, 243, DateTimeKind.Local).AddTicks(9882), "Ward49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 5, 28, 17, 35, 32, 256, DateTimeKind.Local).AddTicks(2458), "Nova83@hotmail.com", "QuuDNSRUH5iSXSRM2JtgCxb9LA8aPFK8UhexVbdafRw=", "ZuqUob6DzXNkvNl+CiUaP02Excm2phlvYsyouaWvn3s=", new DateTime(2020, 5, 28, 17, 35, 32, 256, DateTimeKind.Local).AddTicks(2472), "Waylon7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 268, DateTimeKind.Local).AddTicks(6084), "Scot28@gmail.com", "1MJ9b8FYydyovRHRDOdoDQSxP1rAWz2184dvPV1mtEE=", "0gKctbv+J1sY0eYUFzSpxUAeRJZJap3lxvRv2EJNSio=", new DateTime(2020, 5, 28, 17, 35, 32, 268, DateTimeKind.Local).AddTicks(6112), "Veronica_Crist" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 5, 28, 17, 35, 32, 280, DateTimeKind.Local).AddTicks(9090), "Zachariah_VonRueden48@gmail.com", "C5Ylrb0QdMSxW5eUJRf3Hj8WBjLAbd/2QS004Otf5FU=", "cCctdiieZUeRqi6sjGlS3zu2uLVtf35/JxjzG8JIn+c=", new DateTime(2020, 5, 28, 17, 35, 32, 280, DateTimeKind.Local).AddTicks(9117), "Malachi71" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 293, DateTimeKind.Local).AddTicks(2216), "Geovanni.Wilkinson2@gmail.com", "iwGl4OFSIaaHHPJra+Qtp07qAVPfk7QIXYU3zlMT8ps=", "bPRjVarKHKY5S4WzTdXEjUYJp5jfk7BIazYELJ+HWMQ=", new DateTime(2020, 5, 28, 17, 35, 32, 293, DateTimeKind.Local).AddTicks(2241), "Peggie65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 32, 305, DateTimeKind.Local).AddTicks(4216), "dHDR9Pfmwlz4zcwisavOo5VitwbItbvzunXPgH/8bsg=", "NyLSstd5Nat7Z+zq2KxNPru4tmYYB51aRAIZVxZtAgk=", new DateTime(2020, 5, 28, 17, 35, 32, 305, DateTimeKind.Local).AddTicks(4216) });
        }
    }
}
