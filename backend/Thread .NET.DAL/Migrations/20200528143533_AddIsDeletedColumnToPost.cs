﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddIsDeletedColumnToPost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Posts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 17, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2774), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2781), 2 },
                    { 2, 10, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2019), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2037), 12 },
                    { 3, 7, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2088), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2096), 19 },
                    { 4, 20, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2131), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2139), 18 },
                    { 5, 15, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2173), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2180), 13 },
                    { 6, 6, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2214), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2221), 4 },
                    { 7, 14, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2255), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2262), 15 },
                    { 8, 2, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2296), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2303), 6 },
                    { 9, 14, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2336), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2343), 16 },
                    { 10, 10, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2376), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2383), 2 },
                    { 1, 13, new DateTime(2020, 5, 28, 17, 35, 32, 354, DateTimeKind.Local).AddTicks(9976), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(1186), 12 },
                    { 12, 2, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2455), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2462), 9 },
                    { 13, 13, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2494), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2501), 2 },
                    { 14, 16, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2535), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2542), 7 },
                    { 15, 6, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2574), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2582), 21 },
                    { 16, 9, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2614), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2621), 6 },
                    { 17, 5, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2654), true, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2661), 12 },
                    { 18, 13, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2694), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2701), 3 },
                    { 19, 6, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2734), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2741), 9 },
                    { 11, 2, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2416), false, new DateTime(2020, 5, 28, 17, 35, 32, 355, DateTimeKind.Local).AddTicks(2422), 15 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Nostrum consectetur animi dicta nesciunt veritatis quos similique et beatae.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(2964), 10, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(4137) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Eaque sunt eligendi.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5062), 7, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5080) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Eos occaecati eligendi.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5168), 8, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5176) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Enim incidunt sed aut.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5278), 1, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5286) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Maxime dolorem laboriosam quaerat distinctio rerum iure nobis aut quisquam.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5385), 11, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5393) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Aperiam nostrum id repudiandae consequuntur exercitationem.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5466), 19, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5473) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Rem animi esse quia et nulla.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5544), 8, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5551) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Nulla eaque autem qui vero optio.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5621), 1, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5628) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Rerum qui magni et animi ut a voluptatem voluptatem et.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5726), 20, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5734) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Quas quasi minima optio quod voluptas blanditiis voluptatibus.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5856), 18, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5864) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Magnam magnam quas veritatis nemo iusto et animi.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5940), 7, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(5947) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Sint sit libero alias dolor dolore facere.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6019), 5, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6026) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Quos sit sed corporis.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6086), 4, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6093) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Voluptatum sunt voluptatibus voluptate ut dolores omnis ut minus saepe.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6184), 12, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6191) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Id debitis sit sint omnis hic accusantium quod.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6269), 5, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6276) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Porro est totam possimus.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6337), 20, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6344) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Vero ad hic consequuntur non aliquid sit reiciendis fugiat.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6424), 14, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6431) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Ex voluptates ut deleniti natus et.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6498), 13, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6506) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Dolores dolores numquam vel commodi possimus est saepe.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6581), 16, new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6588) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 11, "Voluptas dolores maiores ipsam est.", new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6661), new DateTime(2020, 5, 28, 17, 35, 32, 334, DateTimeKind.Local).AddTicks(6668) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 910, DateTimeKind.Local).AddTicks(335), "https://s3.amazonaws.com/uifaces/faces/twitter/kevinjohndayy/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(1880) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3560), "https://s3.amazonaws.com/uifaces/faces/twitter/stefvdham/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3578) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3631), "https://s3.amazonaws.com/uifaces/faces/twitter/0therplanet/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3670), "https://s3.amazonaws.com/uifaces/faces/twitter/begreative/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3677) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3705), "https://s3.amazonaws.com/uifaces/faces/twitter/okandungel/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3712) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3741), "https://s3.amazonaws.com/uifaces/faces/twitter/serefka/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3748) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3775), "https://s3.amazonaws.com/uifaces/faces/twitter/quailandquasar/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3782) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3809), "https://s3.amazonaws.com/uifaces/faces/twitter/depaulawagner/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3817) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3845), "https://s3.amazonaws.com/uifaces/faces/twitter/boxmodel/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3852) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3880), "https://s3.amazonaws.com/uifaces/faces/twitter/plasticine/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3887) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3918), "https://s3.amazonaws.com/uifaces/faces/twitter/cynthiasavard/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3925) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3953), "https://s3.amazonaws.com/uifaces/faces/twitter/davidsasda/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3960) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3989), "https://s3.amazonaws.com/uifaces/faces/twitter/lvovenok/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(3996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4023), "https://s3.amazonaws.com/uifaces/faces/twitter/nellleo/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4030) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4058), "https://s3.amazonaws.com/uifaces/faces/twitter/lososina/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4065) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4092), "https://s3.amazonaws.com/uifaces/faces/twitter/thierrykoblentz/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4099) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4127), "https://s3.amazonaws.com/uifaces/faces/twitter/hgharrygo/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4134) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4163), "https://s3.amazonaws.com/uifaces/faces/twitter/mvdheuvel/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4170) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4197), "https://s3.amazonaws.com/uifaces/faces/twitter/carlosjgsousa/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4204) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4233), "https://s3.amazonaws.com/uifaces/faces/twitter/nyancecom/128.jpg", new DateTime(2020, 5, 28, 17, 35, 31, 911, DateTimeKind.Local).AddTicks(4240) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(5739), "https://picsum.photos/640/480/?image=523", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(6995) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7317), "https://picsum.photos/640/480/?image=988", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7333) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7374), "https://picsum.photos/640/480/?image=1050", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7382) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7413), "https://picsum.photos/640/480/?image=761", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7450), "https://picsum.photos/640/480/?image=880", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7457) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7487), "https://picsum.photos/640/480/?image=883", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7494) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7524), "https://picsum.photos/640/480/?image=325", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7531) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7561), "https://picsum.photos/640/480/?image=506", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7568) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7737), "https://picsum.photos/640/480/?image=370", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7744) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7777), "https://picsum.photos/640/480/?image=233", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7784) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7816), "https://picsum.photos/640/480/?image=50", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7823) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7853), "https://picsum.photos/640/480/?image=504", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7860) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7890), "https://picsum.photos/640/480/?image=165", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7897) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7927), "https://picsum.photos/640/480/?image=668", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7934) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7965), "https://picsum.photos/640/480/?image=1055", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(7972) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8001), "https://picsum.photos/640/480/?image=424", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8009) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8038), "https://picsum.photos/640/480/?image=507", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8045) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8074), "https://picsum.photos/640/480/?image=911", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8081) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8111), "https://picsum.photos/640/480/?image=290", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8118) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8148), "https://picsum.photos/640/480/?image=973", new DateTime(2020, 5, 28, 17, 35, 31, 918, DateTimeKind.Local).AddTicks(8155) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(247), false, 19, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(254), 5 },
                    { 11, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(287), false, 2, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(294), 15 },
                    { 12, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(328), true, 18, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(335), 19 },
                    { 13, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(370), true, 5, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(377), 8 },
                    { 15, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(451), true, 15, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(458), 5 },
                    { 18, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(577), true, 4, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(583), 1 },
                    { 16, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(491), true, 2, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(498), 14 },
                    { 17, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(534), true, 13, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(541), 3 },
                    { 9, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(207), false, 8, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(214), 7 },
                    { 20, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(656), true, 12, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(663), 7 },
                    { 19, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(616), true, 5, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(623), 9 },
                    { 14, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(412), true, 11, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(419), 13 },
                    { 8, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(167), false, 2, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(175), 11 },
                    { 3, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9951), true, 7, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9960), 18 },
                    { 6, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(84), true, 3, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(92), 8 },
                    { 5, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(42), true, 7, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(49), 5 },
                    { 1, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(7743), false, 12, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(8937), 16 },
                    { 4, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(1), true, 19, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(8), 4 },
                    { 2, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9841), true, 2, new DateTime(2020, 5, 28, 17, 35, 32, 344, DateTimeKind.Local).AddTicks(9858), 20 },
                    { 7, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(126), false, 4, new DateTime(2020, 5, 28, 17, 35, 32, 345, DateTimeKind.Local).AddTicks(133), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 15, "a", new DateTime(2020, 5, 28, 17, 35, 32, 322, DateTimeKind.Local).AddTicks(1273), new DateTime(2020, 5, 28, 17, 35, 32, 322, DateTimeKind.Local).AddTicks(2512) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Et iste quia molestiae aut facilis. Autem consequatur voluptatibus facilis eum deserunt nulla suscipit et. Perspiciatis delectus et quidem. Consequatur aut delectus eos nihil enim molestiae.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(444), 38, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(477) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Ipsam esse quae excepturi aperiam excepturi inventore.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3502), 29, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3523) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Eaque reprehenderit eum odit quisquam quo repudiandae voluptatem pariatur.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3669), 37, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3677) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Aspernatur praesentium ratione earum quas hic molestiae est.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3769), 27, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3777) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Quis qui quia aperiam et dolore.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3864), 22, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3872) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "quasi", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3972), 32, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(3980) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Ut odit laudantium placeat error architecto ullam esse.
Commodi sed ut dolorum blanditiis accusantium nihil.
Unde tempore et maiores autem fugit earum.
Soluta nesciunt nobis accusantium necessitatibus aut laboriosam.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6159), 40, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6180) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Architecto quo ex doloribus ipsam omnis libero temporibus ut. Est aperiam consequatur consequatur corrupti molestiae. Est dolor doloribus. Sit rerum repudiandae laborum iure soluta aliquam. Earum vel et voluptate quo quos quo ipsam odio.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6609), 34, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6618) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Numquam ut iusto est.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6692), 34, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6700) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Excepturi dolor et nisi saepe eum dignissimos autem molestiae.
Esse aut ea et tempora et provident excepturi et.
Cum illo itaque ut quis delectus est.
Sapiente minus quibusdam harum commodi aut.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6920), 38, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(6928) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Consequatur officiis et maxime quis aperiam qui. Ipsum quia sit quia quis aut aut neque blanditiis. Dicta ut molestiae dolor sapiente velit adipisci quasi dolor. Corporis adipisci molestias at ipsa repudiandae facilis id.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7138), 33, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7146) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, @"Sequi deleniti enim earum.
Molestiae distinctio natus earum velit officiis est.
Cum eum voluptates et omnis ullam magnam.
Molestias aut optio dolorem vitae blanditiis.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7323), 27, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Culpa quod assumenda optio eligendi.
Officia rerum quia dicta ea repellendus.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7435), 33, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7442) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Dolorem sapiente omnis ut quaerat minus est molestias sunt.
Cupiditate aut sit quos reprehenderit ut mollitia aut omnis.
Magni eius sit sapiente expedita consequuntur velit voluptatem consectetur veritatis.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7624), 21, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7632) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "excepturi", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7678), 25, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7685) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 3, "Voluptatem consequatur quia.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7746), new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7754) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 17, @"Autem deserunt delectus sint eaque ab quod sit.
Id omnis quibusdam est magni quia dolores ipsam.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7874), new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(7881) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, "Dolore aut aut officia temporibus est repellendus itaque aut. Ut doloribus suscipit illum sit tempore. Tenetur qui rerum. Rerum consequatur officia qui consectetur nobis fuga officiis est. Aspernatur voluptates aut ut neque harum ducimus voluptatem ut unde. Et quis saepe dolorum quibusdam quas enim facilis.", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8157), new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8164) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "officiis", new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8209), 40, new DateTime(2020, 5, 28, 17, 35, 32, 325, DateTimeKind.Local).AddTicks(8216) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 5, 28, 17, 35, 32, 55, DateTimeKind.Local).AddTicks(781), "Rebeca30@gmail.com", "RyvonGHE/Pjdxfgl5qiJrjbAEjnwvMkoAC2x877hpFA=", "Tu8+jVko0KAd/Q8nJk8uLZub2w+kn9PEVlrdIFhBHIc=", new DateTime(2020, 5, 28, 17, 35, 32, 55, DateTimeKind.Local).AddTicks(2063), "Clara92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 5, 28, 17, 35, 32, 68, DateTimeKind.Local).AddTicks(727), "Roderick_Roob4@gmail.com", "8+XT0XcrvXcdZlMAyVFYfV5f/BDKqlSnFppPyVD+cLk=", "Wk6u7URMjtOxz3jx59rPZFJr8zc2H/Dez1hFh4gTSbM=", new DateTime(2020, 5, 28, 17, 35, 32, 68, DateTimeKind.Local).AddTicks(773), "Davin_Kilback29" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 5, 28, 17, 35, 32, 80, DateTimeKind.Local).AddTicks(5989), "Cody29@yahoo.com", "1cC8bbkdUk2w5HamP/UrNThOPZvuocUqgA0OIUCOh2Q=", "Ilcz1KWXXvgOy9NVPuv7wyW5VYhvigk5eKp/AP36X6A=", new DateTime(2020, 5, 28, 17, 35, 32, 80, DateTimeKind.Local).AddTicks(6027), "Jeanie.Christiansen" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 5, 28, 17, 35, 32, 93, DateTimeKind.Local).AddTicks(1344), "Ora.Nienow@gmail.com", "Ul4GWuqjSLTX653Te31M11UGehESwC4TfpXi92PHAl8=", "GBt9i72yHVEonOQ7yODXXTgg7CqkLeLp2YhnQJySavY=", new DateTime(2020, 5, 28, 17, 35, 32, 93, DateTimeKind.Local).AddTicks(1380), "Ansley_Kuphal" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 5, 28, 17, 35, 32, 105, DateTimeKind.Local).AddTicks(4530), "Florida_Sanford@gmail.com", "kYzPa+P9ckxCQTdf3kDShgD0pznl5Jz9EvG4cJotS84=", "TqyZL1ga8x1BpqweXtrGrKdF2M5DGRuHm2oYNGFzyVE=", new DateTime(2020, 5, 28, 17, 35, 32, 105, DateTimeKind.Local).AddTicks(4546), "Sandra_Brekke95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 5, 28, 17, 35, 32, 117, DateTimeKind.Local).AddTicks(7633), "Mark53@hotmail.com", "/wO6LKs7GUwFhTUH5gTeQ85ATAzQhOXTiVbgIHnstWA=", "tecffbB6g+fYb4fkFx8DmpARf23MMfYcmQF3jtg4pwQ=", new DateTime(2020, 5, 28, 17, 35, 32, 117, DateTimeKind.Local).AddTicks(7662), "Jalon23" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 130, DateTimeKind.Local).AddTicks(848), "Lamar97@hotmail.com", "ziLV4QKhaDo4GfMynoq8H/3a8vUUOJHY5yrNowam810=", "08bzRCCF6gUxios0aKhvtxy4YL+YaI5NUClaPNAOqmc=", new DateTime(2020, 5, 28, 17, 35, 32, 130, DateTimeKind.Local).AddTicks(867), "Arely49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 5, 28, 17, 35, 32, 142, DateTimeKind.Local).AddTicks(3795), "Jeramie2@gmail.com", "vz2CtwJ1xi3pKCOP8iAODTM5ktjpvmoZ/MhbI592UoA=", "9oYHIm6n4lvKH46uS+ttOLz+4L913FzNqw0e6RtSbQU=", new DateTime(2020, 5, 28, 17, 35, 32, 142, DateTimeKind.Local).AddTicks(3827), "Shany.Padberg63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 5, 28, 17, 35, 32, 154, DateTimeKind.Local).AddTicks(6267), "Rachel6@gmail.com", "eF/Kzf+TkcRB3T5yT3nsQa8H3mTIpghzFpWyuguMZR4=", "VN8PA7BYitUum6BaosNSA0D26xVqK8riDWSV+tic7UM=", new DateTime(2020, 5, 28, 17, 35, 32, 154, DateTimeKind.Local).AddTicks(6280), "Reese64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 32, 166, DateTimeKind.Local).AddTicks(8669), "Dustin5@yahoo.com", "dDKUfhyoVIL0xwL2xtRAuh/osD4oV6OCid0vx+NCXdA=", "r3K4RpOQHZHoBZ9sXE6G8GX1skhAxhHw9++Hz8/1/m4=", new DateTime(2020, 5, 28, 17, 35, 32, 166, DateTimeKind.Local).AddTicks(8681), "Jonathan_Durgan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 5, 28, 17, 35, 32, 179, DateTimeKind.Local).AddTicks(1184), "Madisyn50@yahoo.com", "zh7huxkv6ZxnlG/YKiUPZF5AfP65Xg4Pft4ZzUVYFUE=", "afp22J/V8RrSXy8QtXTCGa3ho7kLdTDx0/ES92n+WPs=", new DateTime(2020, 5, 28, 17, 35, 32, 179, DateTimeKind.Local).AddTicks(1203), "Hoyt_Pfeffer98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 5, 28, 17, 35, 32, 191, DateTimeKind.Local).AddTicks(3625), "Imani58@gmail.com", "+mxgITa4n1fqMbJVs5B2T/elwQFur3RwPXTjtnQPzlE=", "hLlS9pDeqDEn/O4iKfJclsUyPY+airs+Qg0hkpqskt4=", new DateTime(2020, 5, 28, 17, 35, 32, 191, DateTimeKind.Local).AddTicks(3639), "Tristin.Mitchell59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 5, 28, 17, 35, 32, 203, DateTimeKind.Local).AddTicks(6152), "Hilton.Quitzon16@hotmail.com", "yw3eQWq5jzWQfs4m0+sw93AiPDqxVrfLkrLgnkVp5R8=", "MXnjhnaI5lr7s82pHrDfehmc/pb9ewJ/yBWLZRUtBvE=", new DateTime(2020, 5, 28, 17, 35, 32, 203, DateTimeKind.Local).AddTicks(6164), "Kristy32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 5, 28, 17, 35, 32, 215, DateTimeKind.Local).AddTicks(8462), "Brannon_Kertzmann45@yahoo.com", "cIZWwZ232uLJuXgdOX6iIab8XNPtIjVSxNPKiAHwKmI=", "6yilvpIBWha2+qbLQYKTwcMI55fnW5QP6aiXPuWSX7Y=", new DateTime(2020, 5, 28, 17, 35, 32, 215, DateTimeKind.Local).AddTicks(8472), "Graham96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 228, DateTimeKind.Local).AddTicks(2724), "Elvie19@hotmail.com", "gvYMb2UlS6JzvPNJX0iyZBA4sflMuXnE+IArIgIZato=", "b0TDNpCRUE4akEMC4yCE5+ZvL233A47VePLY/oAfDgE=", new DateTime(2020, 5, 28, 17, 35, 32, 228, DateTimeKind.Local).AddTicks(2761), "Mae85" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 5, 28, 17, 35, 32, 243, DateTimeKind.Local).AddTicks(9844), "Justyn_OConner@hotmail.com", "PQ9RCb1BrSuYyC6HUCMlVKhV9SkkTpO9hikhF9NVwwE=", "bz/DJRSxJDFrr2i8+8wuHniZ+LKjVNDPvvAj7aKY++E=", new DateTime(2020, 5, 28, 17, 35, 32, 243, DateTimeKind.Local).AddTicks(9882), "Ward49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 5, 28, 17, 35, 32, 256, DateTimeKind.Local).AddTicks(2458), "Nova83@hotmail.com", "QuuDNSRUH5iSXSRM2JtgCxb9LA8aPFK8UhexVbdafRw=", "ZuqUob6DzXNkvNl+CiUaP02Excm2phlvYsyouaWvn3s=", new DateTime(2020, 5, 28, 17, 35, 32, 256, DateTimeKind.Local).AddTicks(2472), "Waylon7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 268, DateTimeKind.Local).AddTicks(6084), "Scot28@gmail.com", "1MJ9b8FYydyovRHRDOdoDQSxP1rAWz2184dvPV1mtEE=", "0gKctbv+J1sY0eYUFzSpxUAeRJZJap3lxvRv2EJNSio=", new DateTime(2020, 5, 28, 17, 35, 32, 268, DateTimeKind.Local).AddTicks(6112), "Veronica_Crist" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 5, 28, 17, 35, 32, 280, DateTimeKind.Local).AddTicks(9090), "Zachariah_VonRueden48@gmail.com", "C5Ylrb0QdMSxW5eUJRf3Hj8WBjLAbd/2QS004Otf5FU=", "cCctdiieZUeRqi6sjGlS3zu2uLVtf35/JxjzG8JIn+c=", new DateTime(2020, 5, 28, 17, 35, 32, 280, DateTimeKind.Local).AddTicks(9117), "Malachi71" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 5, 28, 17, 35, 32, 293, DateTimeKind.Local).AddTicks(2216), "Geovanni.Wilkinson2@gmail.com", "iwGl4OFSIaaHHPJra+Qtp07qAVPfk7QIXYU3zlMT8ps=", "bPRjVarKHKY5S4WzTdXEjUYJp5jfk7BIazYELJ+HWMQ=", new DateTime(2020, 5, 28, 17, 35, 32, 293, DateTimeKind.Local).AddTicks(2241), "Peggie65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 5, 28, 17, 35, 32, 305, DateTimeKind.Local).AddTicks(4216), "dHDR9Pfmwlz4zcwisavOo5VitwbItbvzunXPgH/8bsg=", "NyLSstd5Nat7Z+zq2KxNPru4tmYYB51aRAIZVxZtAgk=", new DateTime(2020, 5, 28, 17, 35, 32, 305, DateTimeKind.Local).AddTicks(4216) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Posts");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 11, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2190), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2202), 2 },
                    { 2, 18, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(9981), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(9), 6 },
                    { 3, 14, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(115), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(127), 18 },
                    { 4, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(208), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(224), 15 },
                    { 5, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(305), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(321), 2 },
                    { 6, 8, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(402), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(419), 14 },
                    { 7, 10, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(504), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(520), 12 },
                    { 8, 1, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(666), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(682), 19 },
                    { 9, 3, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(779), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(796), 16 },
                    { 10, 13, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(881), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(897), 15 },
                    { 1, 11, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(6993), false, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(8315), 21 },
                    { 12, 20, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1079), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1096), 9 },
                    { 13, 6, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1185), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1197), 1 },
                    { 14, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1282), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1298), 7 },
                    { 15, 20, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1379), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1396), 13 },
                    { 16, 4, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1797), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1821), 12 },
                    { 17, 17, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1915), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1931), 1 },
                    { 18, 7, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2008), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2024), 18 },
                    { 19, 2, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2101), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2113), 10 },
                    { 11, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(978), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(994), 5 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Et minus amet.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(961), 3, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(2177) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sit sint architecto.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4079), 13, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4119) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Ea ad deleniti ut quis officia voluptatibus occaecati.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4379), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4399) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "In eius qui necessitatibus et sapiente quis iure.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4626), 16, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4642) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Dolor doloremque est rerum et quis.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4837), 8, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4853) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Tenetur occaecati omnis dolorem molestiae.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5039), 6, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5056) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Ut tempore ut id et odit temporibus.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5295), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5315) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Qui iste temporibus dolores.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5494), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5514) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Dolores ducimus magni qui nesciunt est quia aut a tempore.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5729), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5749) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Delectus non nihil cumque sed dolores ut impedit.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5952), 19, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5972) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Consequuntur odit tempora omnis vel sunt illum nobis et quia.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6195), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6211) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "At aut veritatis veritatis.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6402), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6418) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Voluptates inventore libero sit illo pariatur.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6657), 1, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6677) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Quam magnam quidem cupiditate ratione id ab eum.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6904), 4, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6925) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "At adipisci expedita dignissimos provident architecto.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7119), 17, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7135) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Architecto deleniti earum.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7285), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7302) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Quasi iusto ipsa.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7460), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7480) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aut maiores eum a quibusdam non.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7837), 6, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7861) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Soluta est animi.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8072), 12, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8092) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 20, "In tempora et voluptatibus ut et consequatur fuga.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8287), new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8303) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 425, DateTimeKind.Local).AddTicks(742), "https://s3.amazonaws.com/uifaces/faces/twitter/exentrich/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(2661) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(5920), "https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(5952) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6046), "https://s3.amazonaws.com/uifaces/faces/twitter/joshaustin/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6058) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6127), "https://s3.amazonaws.com/uifaces/faces/twitter/yecidsm/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6143) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6212), "https://s3.amazonaws.com/uifaces/faces/twitter/aoimedia/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6224) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6293), "https://s3.amazonaws.com/uifaces/faces/twitter/jonkspr/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6305) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6374), "https://s3.amazonaws.com/uifaces/faces/twitter/mutlu82/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6386) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6459), "https://s3.amazonaws.com/uifaces/faces/twitter/mgonto/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6471) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6540), "https://s3.amazonaws.com/uifaces/faces/twitter/karalek/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6552) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6621), "https://s3.amazonaws.com/uifaces/faces/twitter/igorgarybaldi/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6633) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6698), "https://s3.amazonaws.com/uifaces/faces/twitter/alek_djuric/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6715) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6779), "https://s3.amazonaws.com/uifaces/faces/twitter/renbyrd/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6796) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6860), "https://s3.amazonaws.com/uifaces/faces/twitter/mahmoudmetwally/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6873) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6942), "https://s3.amazonaws.com/uifaces/faces/twitter/mattdetails/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6954) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7019), "https://s3.amazonaws.com/uifaces/faces/twitter/baumann_alex/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7100), "https://s3.amazonaws.com/uifaces/faces/twitter/kuldarkalvik/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7116) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7181), "https://s3.amazonaws.com/uifaces/faces/twitter/motionthinks/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7193) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7258), "https://s3.amazonaws.com/uifaces/faces/twitter/urrutimeoli/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7339), "https://s3.amazonaws.com/uifaces/faces/twitter/wesleytrankin/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7355) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7420), "https://s3.amazonaws.com/uifaces/faces/twitter/timmillwood/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7436) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(539), "https://picsum.photos/640/480/?image=393", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(1638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2112), "https://picsum.photos/640/480/?image=1079", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2141) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2218), "https://picsum.photos/640/480/?image=801", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2234) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2299), "https://picsum.photos/640/480/?image=951", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2315) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2376), "https://picsum.photos/640/480/?image=436", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2392) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2457), "https://picsum.photos/640/480/?image=64", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2473) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2538), "https://picsum.photos/640/480/?image=436", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2639), "https://picsum.photos/640/480/?image=243", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2656) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2720), "https://picsum.photos/640/480/?image=515", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2733) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2797), "https://picsum.photos/640/480/?image=290", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2814) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2879), "https://picsum.photos/640/480/?image=400", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2891) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2956), "https://picsum.photos/640/480/?image=839", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2972) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3037), "https://picsum.photos/640/480/?image=926", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3053) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3118), "https://picsum.photos/640/480/?image=163", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3130) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3195), "https://picsum.photos/640/480/?image=455", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3207) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3272), "https://picsum.photos/640/480/?image=688", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3284) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3349), "https://picsum.photos/640/480/?image=759", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3361) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3430), "https://picsum.photos/640/480/?image=307", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3442) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3507), "https://picsum.photos/640/480/?image=488", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3523) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3584), "https://picsum.photos/640/480/?image=902", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3600) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4275), false, 5, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4287), 15 },
                    { 11, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4364), false, 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4381), 8 },
                    { 12, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4458), false, 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4470), 2 },
                    { 13, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4543), false, 10, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4559), 16 },
                    { 15, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4721), false, 11, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4733), 12 },
                    { 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5001), true, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5017), 5 },
                    { 16, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4806), true, 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4822), 16 },
                    { 17, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4904), false, 14, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4916), 15 },
                    { 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4186), false, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4198), 5 },
                    { 20, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5199), false, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5216), 19 },
                    { 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5098), false, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5114), 10 },
                    { 14, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4632), true, 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4644), 20 },
                    { 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4097), true, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4113), 20 },
                    { 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3606), false, 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3623), 5 },
                    { 6, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3906), false, 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3918), 13 },
                    { 5, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3809), true, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3821), 16 },
                    { 1, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(465), false, 2, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(1693), 1 },
                    { 4, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3716), true, 6, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3728), 5 },
                    { 2, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3452), false, 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3485), 13 },
                    { 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4004), true, 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4020), 18 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 4, "Enim ea sunt eos reprehenderit maxime.", new DateTime(2019, 6, 8, 13, 30, 1, 808, DateTimeKind.Local).AddTicks(9179), new DateTime(2019, 6, 8, 13, 30, 1, 809, DateTimeKind.Local).AddTicks(346) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Exercitationem assumenda expedita eos possimus dolorem aut repellat eos.
Error voluptate molestiae et laudantium.
Explicabo ea est totam soluta esse cumque voluptatibus qui voluptatem.
Ex corrupti praesentium omnis vitae occaecati.
Dicta doloribus qui vero sit incidunt id aspernatur ipsum.", new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(4781), 23, new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(4826) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Non placeat asperiores qui. Quis qui aut. Tempore iste dolores.", new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(9731), 34, new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(9760) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Atque rerum consequuntur illum rem quia labore.
Vel eos qui et sunt.
Officia aspernatur magnam molestiae.
Laudantium a voluptatibus ut expedita sed et ratione.
Cupiditate unde vitae officia quos.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(307), 31, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(323) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "aperiam", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1361), 29, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1389) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, @"Quisquam nihil natus quidem dolores iusto.
Qui et aspernatur est numquam harum et est totam et.
Accusantium itaque vel eum dolor asperiores placeat.
Aut voluptatum et.
Ut neque impedit voluptatum voluptatem vel aspernatur error nihil.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1941), 35, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1957) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "sed", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2131), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2147) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Voluptas praesentium similique et voluptates unde illum libero necessitatibus facilis.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2387), 32, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2403) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Quos voluptas mollitia magni est quas.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2581), 24, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2597) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Consequuntur ipsam est. Quo autem dolor rem quia quia perferendis et. Inventore laborum saepe. Quis dolorem provident cum a dolore voluptas et occaecati.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3027), 38, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3047) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Dolorum adipisci facere repellendus incidunt quia maiores. Pariatur iure esse nisi. Voluptatum eum error quasi repellat. Aliquid et tempore quo quis dolorem quasi. Aut consectetur itaque aut optio quis soluta qui corporis.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3530), 26, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3550) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Qui adipisci voluptatem ratione voluptatem laborum doloribus commodi.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3741), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3757) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "In distinctio hic ex. Et neque dignissimos odit maiores repudiandae. Quisquam ut in vitae non minima.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4065), 36, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4081) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Aliquid eligendi aut omnis vitae in et ut. Voluptas provident aut sit. Hic ut et eius et quo. Voluptate minus ut facilis. Veniam dignissimos cum doloribus deserunt et.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4523), 38, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4539) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Et vitae ipsa velit facere.
Est consectetur asperiores natus rerum culpa autem qui in.
Illum totam rerum a similique voluptates libero sed.
Iste neque ipsum quis odit doloremque.
Quis harum officia dolores sed sint blanditiis facilis dicta.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5058), 28, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5074) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Corporis nulla adipisci natus ab occaecati et nihil cumque. Velit et id adipisci. Deleniti ut necessitatibus provident autem nostrum maxime. Voluptatem deleniti corporis perferendis ullam officiis eius. Qui voluptatum mollitia aliquam voluptatem.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5545), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5561) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 13, "molestias", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5674), new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5690) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 5, "Minima omnis quaerat ab est ducimus omnis voluptatem. Sunt iste nostrum non vitae placeat molestias ex eum. Saepe deserunt dignissimos qui earum minima. Quia fugiat voluptatem id quo illum sed.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6116), new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6132) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 12, @"Ut assumenda et et.
Voluptatem optio doloremque ipsa rerum nemo repellat quo.
Dignissimos ut magni fugiat quidem voluptatem est aut enim.
Ullam et eos quod ipsum reprehenderit rerum tempora possimus aut.
Porro reprehenderit explicabo illum sed et dolore.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6643), new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6659) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Sapiente quis culpa velit commodi rerum iusto voluptatum neque.
Sunt sed eum nesciunt a vero.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6931), 34, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6943) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 8, 13, 30, 1, 498, DateTimeKind.Local).AddTicks(4064), "Lewis66@yahoo.com", "ipp+S75e/5LiuvKBM0nZrEQsP+7SMRMzqcaRN08Npeo=", "euuwrkqYzcRNJGdeN7pEtbfzLCvqO+eavPQlg+vBgT4=", new DateTime(2019, 6, 8, 13, 30, 1, 498, DateTimeKind.Local).AddTicks(5085), "Berta.Mann13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 8, 13, 30, 1, 510, DateTimeKind.Local).AddTicks(8861), "Zula.Schultz0@gmail.com", "P8ThNsvBpgNPvmH1YqSUD2/vtQRDH/Lul3MQa1elkRI=", "oI1mrCZQsTc837g5oIG3HMPEvL9DrfDpegteKB4mOas=", new DateTime(2019, 6, 8, 13, 30, 1, 510, DateTimeKind.Local).AddTicks(8946), "Emiliano_Rice44" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 8, 13, 30, 1, 523, DateTimeKind.Local).AddTicks(1692), "Reagan60@yahoo.com", "0seLIYhjNnliKhex2dDOLL/USB49B2vjmm35kPNClV0=", "2VgpQTnja97w2gvln6xsIeJnhGCvbckR1fahcmznjBw=", new DateTime(2019, 6, 8, 13, 30, 1, 523, DateTimeKind.Local).AddTicks(1773), "Keely_Johnston" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 535, DateTimeKind.Local).AddTicks(2230), "Keon.Lang62@yahoo.com", "6U7+IRUGC7b0o9gvk13Hmjq7fqhQrz5LUeBrI7N8KFc=", "yfVqUaYEqo8GpvMHeBTVWEvVt8tbqBlOg6RdI2UT4os=", new DateTime(2019, 6, 8, 13, 30, 1, 535, DateTimeKind.Local).AddTicks(2311), "Odell91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 8, 13, 30, 1, 547, DateTimeKind.Local).AddTicks(4604), "Itzel.Hintz@yahoo.com", "NuSFkBsHxCG6xLWi69+t4D0QQGJ6BVVhb9xbXZLdvuw=", "RhFEqCkXU6TJ/PlnhiUc1oPhH+KaEWnNBGn83ls6cpI=", new DateTime(2019, 6, 8, 13, 30, 1, 547, DateTimeKind.Local).AddTicks(4685), "Elmore_Zieme78" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2019, 6, 8, 13, 30, 1, 563, DateTimeKind.Local).AddTicks(166), "Lonie68@hotmail.com", "9kzThCD61VguYbsRHNtsljedbZNR1l25iA5h7tnglD4=", "urBn05zOW2W/z0SRh+ROKzQA8zstGURtGXA8s6Vb2a4=", new DateTime(2019, 6, 8, 13, 30, 1, 563, DateTimeKind.Local).AddTicks(255), "Alanis58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 578, DateTimeKind.Local).AddTicks(2826), "Kristin97@gmail.com", "0V7MfOMBFz35VAxpeLP/VykoHmFr7Y6TMb7KmCsI/aY=", "7sMIRbE3J8XKIl6X9jKvzdPbJKIBm2ObprWHVvqbjCg=", new DateTime(2019, 6, 8, 13, 30, 1, 578, DateTimeKind.Local).AddTicks(3337), "Mallory.Lowe" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2019, 6, 8, 13, 30, 1, 591, DateTimeKind.Local).AddTicks(1073), "Brock.Morissette45@yahoo.com", "W0C3ON2PXLVca3hpSsshp9A731PcJZ/uhs6iqIEPNO0=", "rfSm1BxD/Cz3A0fKq9SM3lViajI8Rf0JUblIsycGTHk=", new DateTime(2019, 6, 8, 13, 30, 1, 591, DateTimeKind.Local).AddTicks(1166), "Adrian47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2019, 6, 8, 13, 30, 1, 603, DateTimeKind.Local).AddTicks(309), "Chester.Botsford18@hotmail.com", "TD3rl13TivQ0isdJJdv3otUiNVS1qu95yZQACMf2xlw=", "D+kG+ztr9Id+AD59gxJdJygPRlt/ioTnMqMBlmFDyjE=", new DateTime(2019, 6, 8, 13, 30, 1, 603, DateTimeKind.Local).AddTicks(394), "Alysha_Kovacek" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 617, DateTimeKind.Local).AddTicks(9451), "Marcelina62@yahoo.com", "aLjZF/bDcOOeW7qCP9dwuSPumeOm9QRufFkUKft46+4=", "bh5Ft5t/KbW9P4fmQXyGpy+XgKou4wo7+Rdd1ySJm8Y=", new DateTime(2019, 6, 8, 13, 30, 1, 617, DateTimeKind.Local).AddTicks(9552), "Rupert_Kassulke55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 639, DateTimeKind.Local).AddTicks(2043), "Shaylee79@yahoo.com", "Rh0T1eDmyKZIDhDBfPV7+xzjf1K+T84PENeigQVqr+E=", "bYVeC1RbOLLbVVmDf1eTE1dAGCKYfgEgYbNWtgYNIyk=", new DateTime(2019, 6, 8, 13, 30, 1, 639, DateTimeKind.Local).AddTicks(2132), "Gwendolyn_Boehm" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 661, DateTimeKind.Local).AddTicks(877), "Mauricio.Von33@yahoo.com", "UTni2loimGZ5Kti93HaEZ+fciZCwfO/ZaPOc44RxhhM=", "cYSMaR0B14+YVoFvlt8tjpPoZY2z5K5jIFb2xsHfxZs=", new DateTime(2019, 6, 8, 13, 30, 1, 661, DateTimeKind.Local).AddTicks(966), "Coleman_Bosco95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 8, 13, 30, 1, 673, DateTimeKind.Local).AddTicks(2489), "Elton.Farrell22@hotmail.com", "DSLPWWQIc4TRCSD6QevyvmOV2Q8UikaYj+CawmvEYhM=", "Nd3AGdFKVWNX6lV8EepGfNXRRTrIbZt+crUIxwN7K4A=", new DateTime(2019, 6, 8, 13, 30, 1, 673, DateTimeKind.Local).AddTicks(2582), "Delores_Hauck92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 8, 13, 30, 1, 685, DateTimeKind.Local).AddTicks(5677), "Sabryna13@hotmail.com", "2I1TP1TVsZiysC0zeVLdGzmL6bsTG5Yro1ZMoZPOVZs=", "2ttqWS2Z+EZN+C1+MQpRFtwsg1kh/bqEJOhp5GaQdo0=", new DateTime(2019, 6, 8, 13, 30, 1, 685, DateTimeKind.Local).AddTicks(5778), "Lucie32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 6, 8, 13, 30, 1, 702, DateTimeKind.Local).AddTicks(7746), "Ardith.Cummings95@yahoo.com", "XKm4g0rS/kMzXDlq1A0xd0Mj1UTKz78QZqEIjGiVzaI=", "joVmnahae87VaRCkiCZANdQn93d2cMmII5sssUek7AQ=", new DateTime(2019, 6, 8, 13, 30, 1, 702, DateTimeKind.Local).AddTicks(7844), "Benton99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 6, 8, 13, 30, 1, 724, DateTimeKind.Local).AddTicks(7786), "Wilfrid54@yahoo.com", "dymXvZziyvE784wDCRRuv+Kazm4duf2yUAZQJG9MFzw=", "6iWfzQsuyR5S7/Xx02k+yJV5y6UWvnNU4rUmZYTba58=", new DateTime(2019, 6, 8, 13, 30, 1, 724, DateTimeKind.Local).AddTicks(7875), "Shaylee.King" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 741, DateTimeKind.Local).AddTicks(6309), "Harold.Rowe71@yahoo.com", "LoU3WAtUBJTUjad+UJqki5rZ6n/M9omgb/3+yVk7vMw=", "n/uUFtMkyUefMnQNz/j6Vaqzr/osn7pBREHzWM8lIU8=", new DateTime(2019, 6, 8, 13, 30, 1, 741, DateTimeKind.Local).AddTicks(6406), "Cali.Waters" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 8, 13, 30, 1, 753, DateTimeKind.Local).AddTicks(8333), "Samanta_OReilly95@yahoo.com", "+YA8dDK1ZoLFTvfpnVQGzmhKXHRaAgN/fQjmZMVMGcM=", "Bhn9S4MMVApZjnGJUamRVV4ikTzCgdc/KBbTMldMsjY=", new DateTime(2019, 6, 8, 13, 30, 1, 753, DateTimeKind.Local).AddTicks(8426), "Baylee_Sipes42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 765, DateTimeKind.Local).AddTicks(8563), "Rex64@hotmail.com", "JOICOM6OozRPh4VLEbxS/iJMsI1HhHyOdDtuG4tau78=", "EdoXXUo+awEKlbwKaH/IFKIzz1Ybyl69YYeYJACQ/nM=", new DateTime(2019, 6, 8, 13, 30, 1, 765, DateTimeKind.Local).AddTicks(8648), "Sharon32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 8, 13, 30, 1, 779, DateTimeKind.Local).AddTicks(1504), "Brice_Abshire72@gmail.com", "D+HiHDuS3LIsqdZUuSEQeZ0VSpvsWiTYTKiW1JwGMPU=", "unJ1mBlcdh5pmmYITwm9s49RsQknqWgoUYsqdrOvjbI=", new DateTime(2019, 6, 8, 13, 30, 1, 779, DateTimeKind.Local).AddTicks(1590), "Kayla46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 791, DateTimeKind.Local).AddTicks(1782), "i2TJ0z00NYlgYE1RckEz65RuduHNOi6XLzkY/zjls3M=", "OfXWhBQuNh3ZiHzV/LYq5X7zvFdI56jXiAJ/cQcbvJM=", new DateTime(2019, 6, 8, 13, 30, 1, 791, DateTimeKind.Local).AddTicks(1782) });
        }
    }
}
