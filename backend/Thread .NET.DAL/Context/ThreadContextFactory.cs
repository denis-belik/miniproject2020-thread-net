﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Thread_.NET.DAL.Context
{
    class ThreadContextFactory : IDesignTimeDbContextFactory<ThreadContext>
    {
        public ThreadContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ThreadContext>();

            ConfigurationBuilder builder = new ConfigurationBuilder();
            string path = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;           
            builder.AddJsonFile(path + @"\Thread .NET.WebAPI\appsettings.json");
            IConfigurationRoot config = builder.Build();

            string connectionString = config.GetConnectionString("ThreadDBConnection");
            optionsBuilder.UseSqlServer(connectionString, opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            return new ThreadContext(optionsBuilder.Options);
        }
    }
}
