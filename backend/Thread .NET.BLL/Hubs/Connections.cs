﻿using System.Collections.Generic;

namespace Thread_.NET.BLL.Hubs
{
    // Clients dictionary contains user id from database and its hub connectionId strings
    public class Connections
    {
        public Dictionary<int, List<string>> Clients { get; set; } = new Dictionary<int, List<string>>();

        private static Connections instance;

        private Connections() { }

        public static Connections GetInstance()
        {
            if (instance == null)
            {
                instance = new Connections();
            }
            return instance;
        }
    }
}
