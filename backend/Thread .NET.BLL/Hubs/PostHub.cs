﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class PostHub : Hub
    {
        Connections Connections = Connections.GetInstance();

        public async Task Send(PostDTO post)
        {
            await Clients.All.SendAsync("NewPost", post);
        }

        // when user logs in, "MapClient" method is sent 
        // and its connectionId string maps to its id 
        public void MapClient(int userId)
        {     
            if(Connections.Clients.ContainsKey(userId))
            {
                if (!Connections.Clients[userId].Contains(Context.ConnectionId))
                {
                    Connections.Clients[userId].Add(Context.ConnectionId);
                }   
            }
            else
            {
                Connections.Clients.Add(userId, new List<string>() { Context.ConnectionId });
            }           
        }

        // when user log out, "RemoveClient" method is send
        // and its connectionId string is being removed from collection
        public void RemoveClient(int userId)
        {
            Connections.Clients[userId].Remove(Context.ConnectionId);

            if (Connections.Clients[userId].Count == 0)
            {
                Connections.Clients.Remove(userId);
            }
        }

        // when client has disconnected, Disconnected event is fired
        // and its connectionId string is being removed from collection
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            Func<int> getUserIdByConnectionId = () =>
            {
                foreach (var client in Connections.Clients)
                {
                    foreach (var connection in client.Value)
                    {
                        if (connection == Context.ConnectionId)
                        {
                            return client.Key;
                        }
                    }
                }
                throw new NotFoundException(nameof(User));
            };

            int userId = getUserIdByConnectionId();

            Connections.Clients[userId].Remove(Context.ConnectionId);
            if (Connections.Clients[userId].Count == 0)
            {
                Connections.Clients.Remove(userId);
            }
        }

        public IReadOnlyList<string> GetUserConnections(int userId)
        {
            if(Connections.Clients.ContainsKey(userId))
            {
                return Connections.Clients[userId];
            }
            else
            {
                return new List<string>();
            }          
        }
    }   
}
