﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.BLL.Exceptions
{
    class AlreadyExistsException : Exception
    {
        public AlreadyExistsException(string entity, string entityName)
            : base($"{entity} {entityName} already exists.")
        {
        }
    }
}
