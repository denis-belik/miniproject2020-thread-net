﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task DeleteComment(int commentId)
        {
            var commentEntity = await _context.Comments
                                .FirstOrDefaultAsync(comment => comment.Id == commentId);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            commentEntity.IsDeleted = true;
            _context.Comments.Update(commentEntity);

            await _context.SaveChangesAsync();
        }

        public async Task UpdateComment(CommentUpdateDTO commentUpdate)
        {
            var commentEntity = await _context.Comments.FirstOrDefaultAsync(c => c.Id == commentUpdate.Id);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment));
            }

            if(string.IsNullOrWhiteSpace(commentUpdate.Body))
            {
                throw new ArgumentException("Empty comment.");
            }

            commentEntity.Body = commentUpdate.Body;

            var timeNow = DateTime.Now;
            commentEntity.UpdatedAt = timeNow;

            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
        }
    }
}
