﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Mail;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly PostHub _postHub;
        private readonly IHubContext<PostHub> _hubContext;
        private readonly MailService _mailService;

        public LikeService(ThreadContext context, IMapper mapper, IHubContext<PostHub> hubContext, PostHub postHub, MailService mailService) : base(context, mapper) 
        {
            this._hubContext = hubContext;
            this._postHub = postHub;
            this._mailService = mailService;
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var reactions = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);
            var sameReactions = reactions.Where(x => x.IsLike == reaction.IsLike);
            var oppositeReactions = reactions.Where(x => x.IsLike != reaction.IsLike);

            if (sameReactions.Any())
            {
                _context.PostReactions.RemoveRange(sameReactions);
                await _context.SaveChangesAsync();

                // signalR section
                await LikedPostNotify(reaction);
                return;
            }
            else if(oppositeReactions.Any())
            {
                var reactionEntity = oppositeReactions.FirstOrDefault();

                reactionEntity.IsLike = !reactionEntity.IsLike;
                reactionEntity.UpdatedAt = DateTime.Now;

                _context.PostReactions.Update(oppositeReactions.FirstOrDefault());
                await _context.SaveChangesAsync();

                // signalR section
                await LikedPostNotify(reaction);
                return;
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();

           // signalR section
            await LikedPostNotify(reaction);
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var reactions = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);
            var sameReactions = reactions.Where(x => x.IsLike == reaction.IsLike);
            var oppositeReactions = reactions.Where(x => x.IsLike != reaction.IsLike);

            if (sameReactions.Any())
            {
                _context.CommentReactions.RemoveRange(sameReactions);
                await _context.SaveChangesAsync();

                return;
            }
            else if (oppositeReactions.Any())
            {
                var reactionEntity = oppositeReactions.FirstOrDefault();

                reactionEntity.IsLike = !reactionEntity.IsLike;
                reactionEntity.UpdatedAt = DateTime.Now;

                _context.CommentReactions.Update(oppositeReactions.FirstOrDefault());
                await _context.SaveChangesAsync();

                return;
            }

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }

        // Send method to all clients that post author has 
        public async Task LikedPostNotify(NewReactionDTO reaction)
        {
            var postAuthor = _context.Posts
                        .Include(post => post.Author)
                        .FirstOrDefault(post => post.Id == reaction.EntityId)
                        .Author;

            if (postAuthor.Id == reaction.UserId)
            {
                return;
            }

            var likedUser = _context.Users.FirstOrDefault(user => user.Id == reaction.UserId);
            var likedUserDto = _mapper.Map<UserDTO>(likedUser);

            var connectionIds = _postHub.GetUserConnections(postAuthor.Id);

            if(connectionIds.Count != 0)
            {
                await _hubContext.Clients.Clients(connectionIds).SendAsync("LikePost", likedUserDto, reaction);
            }
            else
            {
                string message = MailHelper.GetLikedPostMessage(postAuthor.UserName, likedUser.UserName, reaction.EntityId);
                _mailService.Send(postAuthor.Email, $"{likedUser.UserName} liked your post!", message); 
            }
        }
    }
}
