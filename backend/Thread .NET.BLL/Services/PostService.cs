﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var comments = await _context.Comments
                .Include(comment => comment.Reactions)
                .Include(comment => comment.Author)
                .Where(c => c.IsDeleted == false).ToListAsync();

            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => comments)
                .Where(post => post.IsDeleted == false)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetSomePosts(PostsRangeDTO range)
        {
            if(range.SkipNumber >= _context.Posts.Where(post => post.IsDeleted == false).Count())
            {
                throw new Exception("End of the thread");
            }

            var comments = await _context.Comments
                .Include(comment => comment.Reactions)
                .Include(comment => comment.Author)
                .Where(c => c.IsDeleted == false).ToListAsync();

            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => comments)
                .Where(post => post.IsDeleted == false)
                .OrderByDescending(post => post.CreatedAt)
                .Skip(range.SkipNumber)
                .Take(range.LoadNumber)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var comments = await _context.Comments
                .Include(comment => comment.Author)
                .Where(c => c.IsDeleted == false).ToListAsync();

            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => comments)
                .Where(p => p.AuthorId == userId && p.IsDeleted == false) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> GetPostById(int postId)
        {
            var comments = await _context.Comments
                .Include(comment => comment.Author)
                .Where(c => c.IsDeleted == false).ToListAsync();

            var post = await _context.Posts
               .Include(post => post.Author)
                   .ThenInclude(author => author.Avatar)
               .Include(post => post.Preview)
               .Include(post => post.Reactions)
                   .ThenInclude(reaction => reaction.User)
               .Include(post => comments)
               .FirstOrDefaultAsync(post => post.Id == postId && post.IsDeleted == false);

            if(post == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }
            
            var postDto = _mapper.Map<PostDTO>(post);
            return postDto;
        }

        public async Task<ICollection<PostDTO>> GetFavoritePosts(int userId)
        {
            var comments = await _context.Comments
                .Include(comment => comment.Author)
                .Where(c => c.IsDeleted == false).ToListAsync();

            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => comments)
                .Where(p => p.Reactions.Any(r => r.UserId == userId) && p.IsDeleted == false) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task DeletePost(int postId)
        {         
            var postEntity = await _context.Posts.FirstOrDefaultAsync(post => post.Id == postId);

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            postEntity.IsDeleted = true;
            _context.Posts.Update(postEntity);

            await _context.SaveChangesAsync();

            await _postHub.Clients.All.SendAsync("DeletePost", postEntity.Id);
        }      

        public async Task UpdatePost(PostUpdateDTO postUpdateDto)
        {
            var postEntity = await GetPostByIdInternal(postUpdateDto.Id);

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post));
            }

            if (string.IsNullOrWhiteSpace(postUpdateDto.Body) & string.IsNullOrWhiteSpace(postUpdateDto.PreviewImage))
            {
                throw new ArgumentException("Empty post.");
            }

            if (postEntity.Body != postUpdateDto.Body)
            {
                postEntity.Body = postUpdateDto.Body;
            }
            
            if(postEntity.Preview == null)
            {
                if(!string.IsNullOrWhiteSpace(postUpdateDto.PreviewImage))
                {
                    postEntity.Preview = new Image() { URL = postUpdateDto.PreviewImage };
                }
            }
            else
            {
                if(!string.IsNullOrWhiteSpace(postUpdateDto.PreviewImage))
                {
                    postEntity.Preview.URL = postUpdateDto.PreviewImage;
                }
                else
                {
                    _context.Images.Remove(postEntity.Preview);
                    postEntity.PreviewId = null;
                }               
            }

            postEntity.UpdatedAt = DateTime.Now;

            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();
        }

        private async Task<Post> GetPostByIdInternal(int id)
        {
            return await _context.Posts
                .Include(post => post.Preview)
                .FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
