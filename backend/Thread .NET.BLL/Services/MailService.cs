﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.Mail;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public class MailService : BaseService
    {
        private string email = Environment.GetEnvironmentVariable("THREADNET_EMAIL", EnvironmentVariableTarget.User);
        private string password = Environment.GetEnvironmentVariable("THREADNET_EMAIL_PASSWORD", EnvironmentVariableTarget.User);

        private SmtpClient smtpClient;
        private ThreadContext _context;

        public MailService(ThreadContext context, IMapper mapper) :base(context, mapper)
        {
            _context = context;

            smtpClient = new SmtpClient("mail.univ.net.ua");
            smtpClient.Port = 25;
            smtpClient.Credentials = new NetworkCredential(email, password);
            smtpClient.EnableSsl = true;
        }

        public void Send(string receiver, string subject, string messageText)
        {
            var mailMessage = new MailMessage(email, receiver, subject, messageText);

            mailMessage.From = new MailAddress(email, "ThreadNet");
            mailMessage.IsBodyHtml = true;

            smtpClient.SendAsync(mailMessage, null);
        }

        public async Task SharePost(PostShareDTO postShare)
        {
            if (!_context.Posts.Any(post => post.Id == postShare.PostId))
            {
                throw new NotFoundException(nameof(Post), postShare.PostId);
            }

            string email = _context.Users.FirstOrDefault(user => user.UserName == postShare.ReceiverName).Email;
            if (email == null)
            {
                throw new NotFoundException("Email");
            }

            string message = MailHelper.GetSharedPostMessage(postShare.ReceiverName, postShare.SenderName, postShare.PostId);

            this.Send(email, $"{postShare.SenderName} shared a post with you", message);
        }
    }
}
