﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly MailService _mailService;

        public PostsController(PostService postService, LikeService likeService, MailService mailService)
        {
            _postService = postService;
            _likeService = likeService;
            _mailService = mailService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }

        [HttpGet("user/{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> GetByUserId(int id)
        {
            return Ok(await _postService.GetAllPosts(id));
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> GetById(int id)
        {
            return Ok(await _postService.GetPostById(id));
        }

        [HttpGet("favorite/{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> GetFavorite(int id)
        {
            return Ok(await _postService.GetFavoritePosts(id));
        }

        [HttpGet("some")]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> GetSome([FromQuery] PostsRangeDTO range)
        {
            return Ok(await _postService.GetSomePosts(range));
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikePost(reaction);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            await _postService.DeletePost(id);
            return NoContent();
        }

        [HttpPut]
        public async Task<IActionResult> UpdatePost([FromBody] PostUpdateDTO postUpdateDto)
        {
            await _postService.UpdatePost(postUpdateDto);
            return NoContent();
        }

        [HttpPost("share/email")]
        public async Task<IActionResult> SharePostEmail([FromBody] PostShareDTO postShare)
        {
            await _mailService.SharePost(postShare);
            return NoContent();
        }
    }                   
}